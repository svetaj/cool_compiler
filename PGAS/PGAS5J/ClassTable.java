import java.io.PrintStream;
import java.util.Stack;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Enumeration;
import java.util.Arrays;

/** This class may be used to contain the semantic information such as
 * the inheritance graph.  You may use it or not as you like: it is only
 * here to provide a container for the supplied methods.  */
class ClassTable {
    private int semantErrors;
    private PrintStream errorStream;
    private HashMap<AbstractSymbol, class_>  cls_map;
    private HashMap<String, Object[]> mtd_map;
    private HashMap<String, Object[]> att_map;
    private SymbolTable smbt;
    private boolean dbg;
    private int let_level;
    int pass;

    /** Creates data structures representing basic Cool classes (Object,
     * IO, Int, Bool, String).  Please note: as is this method does not
     * do anything useful; you will need to edit it to make if do what
     * you want.
     * */
    private void installBasicClasses() {
	AbstractSymbol filename 
	    = AbstractTable.stringtable.addString("<basic class>");
	
	// The following demonstrates how to create dummy parse trees to
	// refer to basic Cool classes.  There's no need for method
	// bodies -- these are already built into the runtime system.

	// IMPORTANT: The results of the following expressions are
	// stored in local variables.  You will want to do something
	// with those variables at the end of this method to make this
	// code meaningful.

	// The Object class has no parent class. Its methods are
	//        cool_abort() : Object    aborts the program
	//        type_name() : Str        returns a string representation 
	//                                 of class name
	//        copy() : SELF_TYPE       returns a copy of the object

	class_ Object_class = 
	    new class_(0, 
		       TreeConstants.Object_, 
		       TreeConstants.No_class,
		       new Features(0)
			   .appendElement(new method(0, 
					      TreeConstants.cool_abort, 
					      new Formals(0), 
					      TreeConstants.Object_, 
					      new no_expr(0)))
			   .appendElement(new method(0,
					      TreeConstants.type_name,
					      new Formals(0),
					      TreeConstants.Str,
					      new no_expr(0)))
			   .appendElement(new method(0,
					      TreeConstants.copy,
					      new Formals(0),
					      TreeConstants.SELF_TYPE,
					      new no_expr(0))),
		       filename);
	
	// The IO class inherits from Object. Its methods are
	//        out_string(Str) : SELF_TYPE  writes a string to the output
	//        out_int(Int) : SELF_TYPE      "    an int    "  "     "
	//        in_string() : Str            reads a string from the input
	//        in_int() : Int                "   an int     "  "     "

	class_ IO_class = 
	    new class_(0,
		       TreeConstants.IO,
		       TreeConstants.Object_,
		       new Features(0)
			   .appendElement(new method(0,
					      TreeConstants.out_string,
					      new Formals(0)
						  .appendElement(new formal(0,
								     TreeConstants.arg,
								     TreeConstants.Str)),
					      TreeConstants.SELF_TYPE,
					      new no_expr(0)))
			   .appendElement(new method(0,
					      TreeConstants.out_int,
					      new Formals(0)
						  .appendElement(new formal(0,
								     TreeConstants.arg,
								     TreeConstants.Int)),
					      TreeConstants.SELF_TYPE,
					      new no_expr(0)))
			   .appendElement(new method(0,
					      TreeConstants.in_string,
					      new Formals(0),
					      TreeConstants.Str,
					      new no_expr(0)))
			   .appendElement(new method(0,
					      TreeConstants.in_int,
					      new Formals(0),
					      TreeConstants.Int,
					      new no_expr(0))),
		       filename);

	// The Int class has no methods and only a single attribute, the
	// "val" for the integer.

	class_ Int_class = 
	    new class_(0,
		       TreeConstants.Int,
		       TreeConstants.Object_,
		       new Features(0)
			   .appendElement(new attr(0,
					    TreeConstants.val,
					    TreeConstants.prim_slot,
					    new no_expr(0))),
		       filename);

	// Bool also has only the "val" slot.
	class_ Bool_class = 
	    new class_(0,
		       TreeConstants.Bool,
		       TreeConstants.Object_,
		       new Features(0)
			   .appendElement(new attr(0,
					    TreeConstants.val,
					    TreeConstants.prim_slot,
					    new no_expr(0))),
		       filename);

	// The class Str has a number of slots and operations:
	//       val                              the length of the string
	//       str_field                        the string itself
	//       length() : Int                   returns length of the string
	//       concat(arg: Str) : Str           performs string concatenation
	//       substr(arg: Int, arg2: Int): Str substring selection

	class_ Str_class =
	    new class_(0,
		       TreeConstants.Str,
		       TreeConstants.Object_,
		       new Features(0)
			   .appendElement(new attr(0,
					    TreeConstants.val,
					    TreeConstants.Int,
					    new no_expr(0)))
			   .appendElement(new attr(0,
					    TreeConstants.str_field,
					    TreeConstants.prim_slot,
					    new no_expr(0)))
			   .appendElement(new method(0,
					      TreeConstants.length,
					      new Formals(0),
					      TreeConstants.Int,
					      new no_expr(0)))
			   .appendElement(new method(0,
					      TreeConstants.concat,
					      new Formals(0)
						  .appendElement(new formal(0,
								     TreeConstants.arg, 
								     TreeConstants.Str)),
					      TreeConstants.Str,
					      new no_expr(0)))
			   .appendElement(new method(0,
					      TreeConstants.substr,
					      new Formals(0)
						  .appendElement(new formal(0,
								     TreeConstants.arg,
								     TreeConstants.Int))
						  .appendElement(new formal(0,
								     TreeConstants.arg2,
								     TreeConstants.Int)),
					      TreeConstants.Str,
					      new no_expr(0))),
		       filename);

	/* Do somethind with Object_class, IO_class, Int_class,
           Bool_class, and Str_class here */
        if (pass == 0) cls_map.put(Object_class.getName(), Object_class);
        if (pass == 0) cls_map.put(IO_class.getName(), IO_class);
        if (pass == 0) cls_map.put(Int_class.getName(), Int_class);
        if (pass == 0) cls_map.put(Bool_class.getName(), Bool_class);
        if (pass == 0) cls_map.put(Str_class.getName(), Str_class);
        travers_ast(Object_class);
        travers_ast(IO_class);
        travers_ast(Int_class);
        travers_ast(Bool_class);
        travers_ast(Str_class);
    }

    public boolean test_Main() 
    {
        boolean found = false;
        class_ x;
        for (AbstractSymbol key: cls_map.keySet()) {
            x = cls_map.get(key);
            if ( (x.getName().toString()).equals("Main")) {
                found = true;
                break;
            }
        }
        return found;
    }

    // compare actual method parameters types with method definition types
    public boolean actual_formal(Expressions ax, method mx) {
        class_ cc = (class_)smbt.probe(TreeConstants.curr_class);
        if (dbg) System.out.printf("%sActual <--> Formal comparison\n", tab());
        boolean equal = false, err = false;
        Formals fms = mx.formals;
        Enumeration ea = ax.getElements();
        Enumeration ef = fms.getElements();
        if (ax.getLength() == fms.getLength()) {
            if (dbg) System.out.printf("%s    Equal number of arguments.\n", tab());
            while (true) {
                err = false;
                if (!ea.hasMoreElements()) { equal = true; break; }
                Expression ex = (Expression)(ea.nextElement());
                formal fx = (formal)(ef.nextElement());
                AbstractSymbol ta = ex.get_type();
                if (ta.equals(TreeConstants.SELF_TYPE)) ta = cc.getName();
                AbstractSymbol tf = fx.type_decl;
                AbstractSymbol nf = fx.name;
                if (dbg) System.out.printf("%s    Compare types (%s,%s).\n", tab(), ta, tf);
                AbstractSymbol lb = lub(ta, tf);
                if (dbg) System.out.printf("%s    LUB (%s,%s) = %s.\n", tab(), ta, tf, lb);
                //if (ta.equals(TreeConstants.SELF_TYPE) || tf.equals(TreeConstants.SELF_TYPE)) err = true;
                if (!err && !lb.equals(tf)) {
                    semantError(cc).printf("In call of method %s, type %s of parameter %s does not conform to declared type %s.\n", 
                                                mx.name, ta, nf, tf);
                }
            }
        }
        return equal;
    }

    // compare two methods (formal parameters - number and types)
    public boolean formal_formal(method mx1, method mx2) {
        class_ cc = (class_)smbt.probe(TreeConstants.curr_class);
        if (dbg) System.out.printf("%sFormal <--> Formal comparison\n", tab());
        if (dbg) System.out.printf("%sFormal <--> Formal (%s, %s)\n", tab(), mx1.name, mx2.name);
        boolean equal = false, err = false;
        Formals fms1 = mx1.formals;
        Formals fms2 = mx2.formals;
        Enumeration ef1 = fms1.getElements();
        Enumeration ef2 = fms2.getElements();
        if (fms1.getLength() == fms2.getLength()) {
            if (dbg) System.out.printf("%s    Equal number of arguments.\n", tab());
            while (true) {
                err = false;
                if (!ef2.hasMoreElements()) { equal = true; break; }
                formal fx1 = (formal)(ef1.nextElement());
                formal fx2 = (formal)(ef2.nextElement());
                AbstractSymbol tf1 = fx1.type_decl;
                AbstractSymbol nf1 = fx1.name;
                AbstractSymbol tf2 = fx2.type_decl;
                AbstractSymbol nf2 = fx2.name;
                if (dbg) System.out.printf("%s    Compare types (%s,%s).\n", tab(), tf1, tf2);
                if (!err && !tf1.equals(tf2)) {
                    semantError(cc).printf("In redefined method %s, parameter type %s is different from original type %s.\n", 
                                                mx2.name, tf1, tf2);
                }
            }
        } 
        else {
             semantError(cc).printf("Incompatible number of formal parameters in redefined method %s.\n", mx2.name);
        }
        return equal;
    }

    // valid only in pass #2 (all expression evaluated, final types known)
    public AbstractSymbol find_mtd_map(dispatch dp, AbstractSymbol cn) 
    {
         return find_mtd_map(dp.name, dp.actual, cn);
    }

    public AbstractSymbol find_mtd_map(static_dispatch sdp, AbstractSymbol cn) 
    {
         return find_mtd_map(sdp.name, sdp.actual, cn);
    }

    public AbstractSymbol find_mtd_map(AbstractSymbol aa, Expressions ee, AbstractSymbol cn) 
    {
        AbstractSymbol tn = null;
        Object[] x;
        for (String key: mtd_map.keySet()) {
            if (dbg) System.out.printf("%s    find_mtd_map (%s,%s).\n", tab(), aa, key);
            if (!key.equals(aa.toString()+":"+cn.toString())) continue;
            x = mtd_map.get(key);
            method mx = (method)x[0];
            class_ cx = (class_)x[1];
            if (!cn.equals(cx.getName())) continue;
            if (actual_formal(ee, mx)) { 
                tn = mx.return_type;
                break;
            }
        }
        if (tn == null) {
            class_ cc = cls_map.get(cn);
            AbstractSymbol next = cc.getParent();
            if (dbg) System.out.printf("%s    find_mtd_map proceed to parent (%s,%s).\n", tab(), cn, next);
            if (next != null) {
                if (cc != null && !next.equals(TreeConstants.No_class)) {
                    tn = find_mtd_map(aa, ee, next);
                }
            }
        }
        return tn; 
    }


    public boolean check_mtd_map(method mm, AbstractSymbol cn) 
    {
        boolean isok = false;
        Object[] x;
        if (dbg) System.out.printf("%s    check_mtd_map method %s starting from class %s.\n", tab(), mm.name, cn);
        for (String key: mtd_map.keySet()) {
            if (key.equals(mm.name.toString()+":"+cn.toString())) {
                 if (dbg) System.out.printf("%s    check_mtd_map (%s,%s) starting from class %s.\n", tab(), mm.name, key, cn);
                 x = mtd_map.get(key);
                 method mx = (method)x[0];
                 class_ cx = (class_)x[1];
                 if (formal_formal(mm, mx)) isok = true;
                 break;
             }
        }
        class_ cc = cls_map.get(cn);
        AbstractSymbol next = cc.getParent();
        if (next != null) {
             if (cc != null && !next.equals(TreeConstants.No_class)) {
                 if (dbg) System.out.printf("%s    check_mtd_map proceed from class %s to parent %s.\n", tab(), cn, next);
                 isok = check_mtd_map(mm, next);
             }
        }
        return isok; 
    }

    public void list_mtd_map() 
    {
        Object[] x;
        for (String key: mtd_map.keySet()) {
            x = mtd_map.get(key);
            method mx = (method)x[0];
            class_ cx = (class_)x[1];
            System.out.printf("METHOD     %-20s -> %-20s %s\n", key, mx.return_type, cx.getName());
        }
    }

    public AbstractSymbol find_let_map(String kx)
    {
        AbstractSymbol tn = null;
        Object[] x;
        for (String key: att_map.keySet()) {
            if (!key.equals(kx)) continue;
            x = att_map.get(key);
            if (dbg) System.out.printf("%s    find_fml_let (%s,%s).\n", tab(), kx, key);
            AbstractSymbol ax = (AbstractSymbol)x[0];
            class_ cx = (class_)x[1];
            tn = ax;
            break;
        }
        return tn;
    }

    public AbstractSymbol find_fml_map(String kx)
    {
        AbstractSymbol tn = null;
        Object[] x;
        for (String key: att_map.keySet()) {
            if (!key.equals(kx)) continue;
            x = att_map.get(key);
            if (dbg) System.out.printf("%s    find_fml_map (%s,%s).\n", tab(), kx, key);
            formal fx = (formal)x[0];
            class_ cx = (class_)x[1];
            tn = fx.type_decl;
            break;
        }
        return tn;
    }

    // valid only in pass #2 (all expression evaluated, final types known)
    public AbstractSymbol find_att_map(AbstractSymbol at, AbstractSymbol cn) 
    {
        AbstractSymbol tn = null;
        Object[] x;
        for (String key: att_map.keySet()) {
            if (!key.equals(at.toString()+":"+cn.toString())) continue;
            x = att_map.get(key);
            if (dbg) System.out.printf("%s    find_att_map (%s,%s).\n", tab(), at, key);
            attr ax = (attr)x[0];
            class_ cx = (class_)x[1];
            if (cn.equals(cx.getName())) {
                tn = ax.type_decl;
                break;
            }
        }
        if (tn == null) {
            class_ cc = cls_map.get(cn);
            AbstractSymbol next = cc.getParent();
            if (dbg) System.out.printf("%s    find_att_map proceed to parent (%s,%s).\n", tab(), cn, next);
            if (next != null) {
                if (cc != null && !next.equals(TreeConstants.No_class)) {
                    tn = find_att_map(at, next);
                }
            }
        }
        return tn; 
    }


    public void list_att_map() 
    {
        AbstractSymbol tx = null;
        Object[] x;
        for (String key: att_map.keySet()) {
            x = att_map.get(key);
            String nn = x[0].getClass().getName();
            if (nn.equals("attr")) 
                tx = ((attr)x[0]).type_decl;
            else if (nn.equals("formal")) 
                tx = ((formal)x[0]).type_decl;
            class_ cx = (class_)x[1];
            System.out.printf("ATTRIB     %-20s -> %-20s %s\n", key, tx, cx.getName());
        }
    }

    public HashMap<AbstractSymbol,class_> test_basic() 
    {
        class_ x;
        HashMap<AbstractSymbol,class_> mapx =  new HashMap<AbstractSymbol,class_>();
        for (AbstractSymbol key: cls_map.keySet()) {
            x = cls_map.get(key);
            if ( (x.getParent().toString()).equals("Bool") ||
                 (x.getParent().toString()).equals("Int")  ||
                 (x.getParent().toString()).equals("String") )
                mapx.put(key,x);
        }
        return mapx;
    }

    public HashMap<AbstractSymbol,class_> test_missing() 
    {
        class_ x;
        HashMap<AbstractSymbol,class_> mapx =  new HashMap<AbstractSymbol,class_>();
        for (AbstractSymbol key: cls_map.keySet()) {
            x = cls_map.get(key);
            if ((x.getParent().toString()).equals("Object")) continue;
            if (cls_map.get(x.getParent()) != null) continue;
            if ( (x.getParent().toString()).equals("Bool") ||
                 (x.getParent().toString()).equals("Int")  ||
                 (x.getParent().toString()).equals("IO")  ||
                 (x.getParent().toString()).equals("String") ) continue;
            if ((x.getParent()).equals(TreeConstants.No_class)) continue;
            mapx.put(key,x);
        }
        return mapx;
    }

    public boolean test_type(AbstractSymbol as) 
    {
        if ( (as.toString()).equals("Bool") ||
             (as.toString()).equals("Int") ||
             (as.toString()).equals("IO") ||
             (as.toString()).equals("Object") ||
             (as.toString()).equals("SELF_TYPE") ||
             (as.toString()).equals("_prim_slot") ||
             (as.toString()).equals("String") ) return true; 
        for (AbstractSymbol key: cls_map.keySet()) {
            if ((as.toString()).equals(key.toString())) return true;
        }
        return false;
    }

    public HashSet<AbstractSymbol> test_loop()
    {
        class_ x, y;
        AbstractSymbol startx, ax, bx, loopx;
        int sz = cls_map.size();
        int count;
        HashSet<AbstractSymbol> setx =  new HashSet<AbstractSymbol>();

        loopx = null;
        if (sz <= 1) return setx;
        for (AbstractSymbol key: cls_map.keySet()) {
            loopx = null;
            x = cls_map.get(key);
            startx = x.getName();
            ax = startx;
            count = 1;
            while (ax != null) {
                y = cls_map.get(ax);
                if (y == null) break;
                ax = y.getParent();
                count++; 
                if (count > sz || (ax.toString()).equals(startx.toString())) {
                    loopx = startx;
                    break;
                }
            }
            if (loopx != null) {
                setx.add(loopx);
                continue;
            }
        }
        return setx;
    }

    /* least upper bound */
    public AbstractSymbol lub(AbstractSymbol a, AbstractSymbol b)
    {
        Stack<AbstractSymbol> sa = new Stack<AbstractSymbol>();
        Stack<AbstractSymbol> sb = new Stack<AbstractSymbol>();
        AbstractSymbol cx,cy,cold;
        cx = a; 
        while (cx != null) {
           sa.addElement(cx);
           class_ cxx = cls_map.get(cx);
           if (cxx == null) break;
           cx = cxx.getParent();
           if (cx.equals(TreeConstants.Object_)) {sa.addElement(cx); break;}
        }
        cx = b; 
        while (cx != null) {
           sb.addElement(cx);
           class_ cxx = cls_map.get(cx);
           if (cxx == null) break;
           cx = cxx.getParent();
           if (cx.equals(TreeConstants.Object_)) {sb.addElement(cx); break;}
        }
        if (sa.empty()) return null;
        if (sb.empty()) return null;
        cx = sa.pop();
        cy = sb.pop();
        cold = TreeConstants.Object_;
        while (cx != null && cy != null) {
            if (! (cx.toString()).equals(cy.toString()))  break;
            cold = cx;
            if (sa.empty() || sb.empty()) break;
            cx = sa.pop();
            cy = sb.pop();
        }
        return cold;
    }

/* ========================================================================   
		class Cases extends ListNode
		class branch extends Case
		class typcase extends Expression
		class let extends Expression
		class static_dispatch extends Expression
		class dispatch extends Expression
		class assign extends Expression
		class cond extends Expression
		class block extends Expression
		class loop extends Expression
		class comp extends Expression
		class Classes extends ListNode
		class Features extends ListNode
		class Formals extends ListNode
		class Expressions extends ListNode
		class program extends Program
		class class_ extends Class_
		class method extends Feature
		class attr extends Feature
		class formal extends Formal
		class plus extends Expression
		class sub extends Expression
		class mul extends Expression
		class divide extends Expression
		class neg extends Expression
		class lt extends Expression
		class eq extends Expression
		class leq extends Expression
		class int_const extends Expression
		class bool_const extends Expression
		class string_const extends Expression
		class new_ extends Expression
		class isvoid extends Expression
		class no_expr extends Expression
		class object extends Expression
======================================================================== */

/* ======================================================================== */
    int depth;
    String blank_line;

    public String tab()
    {
          return blank_line.substring(0,depth*3);
    }

    /* Program -> Classes -> class_ */
    public void travers_ast(Classes cls) {
        if (dbg) System.out.println(tab()+"Classes");
        for (Enumeration e = cls.getElements(); e.hasMoreElements(); ) {
	    travers_ast((class_)e.nextElement());
            if (semantErrors > 0) break;
        }
    }

    /* class_ -> Features  */
    public void travers_ast(class_ cl) {
        smbt.enterScope();
        if (dbg) System.out.println(tab()+"class_");
        if (dbg) System.out.printf("%s***** ENTER SCOPE ****\n", tab());
        depth++;
        AbstractSymbol nm = cl.name;
        AbstractSymbol pr = cl.parent;
        AbstractSymbol fn = cl.filename;
        if (dbg) System.out.printf("%sclass_ (%s %s %s)\n", tab(), nm, pr, fn);
        smbt.addId(TreeConstants.curr_class,cl);
        if (dbg) System.out.printf("%s***** SCOPE ADD %s ****\n", tab(), TreeConstants.curr_class);
        smbt.addId(cl.name,"__class__");
        if (dbg) System.out.printf("%s***** SCOPE ADD %s ****\n", tab(), cl.name);
        travers_ast(cl.features);
        depth--;
        if (dbg) System.out.printf("%s***** EXIT  SCOPE ****\n", tab());
        smbt.exitScope();
    }

    /* Features -> method | attr */
    public void travers_ast(Features fts) {
        depth++;
        if (dbg) System.out.println(tab()+"Features");
        String cn, cn1;
        class_ cc = (class_)smbt.probe(TreeConstants.curr_class);

        /* pass #1 check dupicates and collect names */
        for (Enumeration e = fts.getElements(); e.hasMoreElements(); ) {
            Object o = e.nextElement();
            cn = o.getClass().getName();
            if (dbg) System.out.printf("%sFeatures (class -> %s)\n", tab(), cn);
            if (cn.equals("method")) {
               Object x = smbt.probe(((method)o).name);
               if (x != null) {
                   cn1 = x.getClass().getName();
                   if (cn1.equals("method"))
                        semantError(cc).printf("Method %s is multiply defined.\n", ((method)o).name);
               }
               else {
                   smbt.addId(TreeConstants.curr_method, (method) o);
                   if (dbg) System.out.printf("%s***** SCOPE ADD %s ****\n", tab(), TreeConstants.curr_method);
                   smbt.addId(((method)o).name, (method)o);
                   if (dbg) System.out.printf("%s***** SCOPE ADD %s ****\n", tab(), ((method)o).name);
               }
               if (pass == 1) {
                   if (!check_mtd_map((method)o, cc.getName()))
                       break;
               }
            }
            else if (cn.equals("attr")) {
               if ( (((attr)o).name).equals(TreeConstants.self) ) {
                   semantError(cc).printf("'self' cannot be the name of an attribute.\n");
                   continue;
               }
               Object x = smbt.probe(((attr)o).name);
               if (x != null) {
                   cn1 = x.getClass().getName();
                   if (cn1.equals("attr"))
                       semantError(cc).printf("Attribute %s is multiply defined in class.\n", ((attr)o).name);
               }
               else {
                   smbt.addId(((attr)o).name, (attr)o);
                   if (dbg) System.out.printf("%s***** SCOPE ADD %s ****\n", tab(), ((attr)o).name);
               }
            }
        }
        smbt.addId(TreeConstants.self, TreeConstants.SELF_TYPE);
        if (dbg) System.out.printf("%s***** SCOPE ADD %s ****\n", tab(), TreeConstants.self);

        for (Enumeration e = fts.getElements(); e.hasMoreElements(); ) {
            Object o = e.nextElement();
            cn = o.getClass().getName();
            if (cn.equals("method")) {
	        travers_ast((method)o); 
            }
            else if (cn.equals("attr")) {
	        travers_ast((attr)o); 
            }
        }
        depth--;
    }

    /* Expressions -> Expression  */
    public void travers_ast(Expressions exs) {
        depth++;
        if (dbg) System.out.println(tab()+"Expressions");
        for (Enumeration e = exs.getElements(); e.hasMoreElements(); ) {
	    travers_ast((Expression)e.nextElement());
        }
        depth--;
    }

    /* Formals -> formal  */
    public void travers_ast(Formals fms) {
        class_ cc = (class_)smbt.probe(TreeConstants.curr_class);
        method mm = (method)smbt.probe(TreeConstants.curr_method);
        depth++;
        if (dbg) System.out.println(tab()+"Formals");
        //smbt.enterScope();
        //if (dbg) System.out.printf("%s***** ENTER SCOPE ****\n", tab());
        for (Enumeration e = fms.getElements(); e.hasMoreElements(); ) {
	    formal fc = (formal)e.nextElement();
            if (fc.name.equals(TreeConstants.self)) {
                 semantError(cc).printf("'%s' cannot be the name of a formal parameter.\n", fc.name);
                 break;
            }
            Object x = smbt.probe(fc.name);
            if (x != null && ((String)x).equals("__formal__")) {
                 semantError(cc).printf("Formal parameter %s is multiply defined.\n", fc.name);
            }
            else {
                smbt.addId(fc.name, "__formal__");
                if (dbg) System.out.printf("%s***** SCOPE ADD %s ****\n", tab(), fc.name);
                Object[] env = new Object[2];
                env[0] = fc;
                env[1] = cc;
                if (pass == 0) {
                     if (fc == null) System.out.println("============================ fc");
                     if (cc == null) System.out.println("============================ cc");
                     if (mm == null) System.out.println("============================ mm");
                     String key = fc.name.toString()+":"+cc.getName().toString()+":"+mm.name.toString();
                     att_map.put(key, env);
                     if (dbg) System.out.printf("%sattr att_map.put(%s:%s:%s)\n", tab(), fc.name, cc.getName(), mm.name);
                }
            }
        }
        //smbt.exitScope();
        //if (dbg) System.out.printf("%s***** EXIT  SCOPE ****\n", tab());
        for (Enumeration e = fms.getElements(); e.hasMoreElements(); ) {
	     formal fc = (formal)e.nextElement();
	     travers_ast(fc);
        }
        depth--;
    }

    /* method ->  */
    public void travers_ast(method mt) {
        class_ cc = (class_)smbt.probe(TreeConstants.curr_class);
        smbt.enterScope();
        if (dbg) System.out.printf("%s***** ENTER SCOPE ****\n", tab());
        smbt.addId(mt.name, mt);
        if (dbg) System.out.printf("%s***** SCOPE ADD %s ****\n", tab(), mt.name);
        smbt.addId(TreeConstants.curr_method, mt);
        if (dbg) System.out.printf("%s***** SCOPE ADD %s ****\n", tab(), TreeConstants.curr_method);
        smbt.addId(TreeConstants.curr_class,cc);
        if (dbg) System.out.printf("%s***** SCOPE ADD %s ****\n", tab(), TreeConstants.self);
        smbt.addId(TreeConstants.self, TreeConstants.SELF_TYPE);
        depth++;
        if (dbg) System.out.println(tab()+"method");
        AbstractSymbol	nm = mt.name;
        AbstractSymbol	rt = mt.return_type;
        if (dbg) System.out.printf("%smethod (name -> %s)\n", tab(), nm);
        if (dbg) System.out.printf("%smethod (return_type -> %s)\n", tab(), rt);
        if (!test_type(rt)) {
             int se = semantErrors;
             semantError(cc).printf("Undefined return type %s in method %s.\n", rt, nm);
             semantErrors = se;
        }
        travers_ast(mt.formals);
        Object[] env = new Object[2];
        env[0] = mt;
        env[1] = cc;
        String key = nm.toString()+":"+cc.getName().toString();
        if (pass == 0) mtd_map.put(key, env);
        travers_ast(mt.expr);
        AbstractSymbol et = mt.expr.get_type();
        if (pass == 1 && et != null && !et.equals(TreeConstants.SELF_TYPE)) {
             if (dbg) System.out.printf("%smethod expression=%s, return=%s.\n", tab(), et, rt);
             AbstractSymbol lb = lub(et, rt);
             if (lb != null) {
                 if (dbg) System.out.printf("%smethod LUB (%s,%s) = %s.\n", tab(), et, rt, lb);
                 if (!lb.equals(rt)) {
                      semantError(cc).printf("Inferred return type %s of method %s does not conform to declared return type %s.\n", et, nm, rt);
                 }
            }
        }        
        smbt.exitScope();
        if (dbg) System.out.printf("%s***** EXIT SCOPE ****\n", tab());
        depth--;
    }

    /* attr ->  */
    public void travers_ast(attr at) {
        class_ cc = (class_)smbt.probe(TreeConstants.curr_class);
        depth++;
        if (dbg) System.out.println(tab()+"attr");
        Expression it = at.init ;
        AbstractSymbol nm = at.name;
        AbstractSymbol td = at.type_decl;
        if (test_type(td)) {
            smbt.addId(nm, td);
            if (dbg) System.out.printf("%s***** SCOPE ADD %s ****\n", tab(), nm);
            if (dbg) System.out.printf("%sattr (%s %s)\n", tab(), nm, td);
            if (pass == 1) {
                AbstractSymbol next = find_att_map(nm, cc.getParent());
                if (next != null) {
                    semantError(cc).printf("Attribute %s is an attribute of an inherited class.\n", nm);
                }
            }
        }
        else {
             if (pass == 1)
                 semantError(cc).printf("Class %s of attribute %s is undefined.\n", td, nm);
        }
        Object o = smbt.probe(nm);
        String cn = o.getClass().getName();
        if (dbg) System.out.printf("%sattr stored as cn =%s=\n", tab(), cn);
        Object[] env = new Object[2];
        env[0] = at;
        env[1] = cc;
        if (pass == 0) {
             String key = nm.toString()+":"+cc.getName().toString();
             att_map.put(key, env);
             if (dbg) System.out.printf("%sattr att_map.put(%s:%s)\n", tab(), nm, cc.getName());
        }
        travers_ast(it);
        depth--;
    }

    /* formal ->  */
    public void travers_ast(formal fm) {
        class_ cc = (class_)smbt.probe(TreeConstants.curr_class);
        depth++;
        if (dbg) System.out.println(tab()+"formal");
        AbstractSymbol nm = fm.name;
        AbstractSymbol td = fm.type_decl;
        if (fm.type_decl.equals(TreeConstants.SELF_TYPE)) {
            semantError(cc).printf("Formal parameter %s cannot have type %s.\n", nm, td);
        }
        else if (!test_type(td)) {
            semantError(cc).printf("Class %s of formal parameter %s is undefined.\n", td, nm);
        }
        if (dbg) System.out.printf("%sformal (%s %s)\n", tab(), nm, td);
        depth--;
    }

    /* object ->  */
    public void travers_ast(object ob) {
        class_ cc = (class_)smbt.probe(TreeConstants.curr_class);
        method mm = (method)smbt.probe(TreeConstants.curr_method);
        depth++;
        if (dbg) System.out.println(tab()+"object");
        AbstractSymbol nm = ob.name;
        AbstractSymbol tp = null;
        AbstractSymbol tp1 = null;
        AbstractSymbol tp2 = null;
        AbstractSymbol tp3 = null;
        AbstractSymbol tp4 = null;
        ob.set_type(TreeConstants.Object_);
        if (pass == 1) {
            Object o = smbt.probe(nm);
            if (o != null) {
                if (dbg) System.out.printf("%sobject %s found in smbt\n", tab(), nm);
                String cn = o.getClass().getName();
                if (dbg) System.out.printf("%sobject class is %s \n", tab(), cn);
                if (cn.equals("method")) {
                    tp1 = ((method)o).return_type;
                }
                else if (cn.equals("attr")) {
                    tp1 = ((attr)o).type_decl;
                }
                else if (cn.equals("java.lang.String")) {
                    //tp1 = TreeConstants.Str;
                    tp1 = null;
                }
                else {
                    tp1 = (AbstractSymbol) o;
                }
                if (tp1 != null) {
                    if (dbg) System.out.printf("%sobject type is %s, (return_type or type_decl)\n", tab(), tp1);
                }
            }
            if (mm != null) {
                 String key = ob.name.toString()+":"+cc.getName().toString()+":"+mm.name.toString();
                 tp2 = find_fml_map(key);
                 if (dbg) System.out.printf("%sobject type is %s, (find_att_map 1)\n", tab(), tp2);
            }
            if (ob != null && cc != null) {
                tp3 = find_att_map(ob.name, cc.getName());
                if (dbg) System.out.printf("%sobject type is %s, (find_att_map 2)\n", tab(), tp3);
                if (mm != null) {
                    String key = ob.name.toString()+":"+cc.getName().toString()+":"+mm.name.toString()+":let";
                    tp4 = find_let_map(key);
                     if (dbg) System.out.printf("%sKEY %s object type is %s, (find_let_map 3)\n", tab(), key, tp4);
                }
            }
            if (let_level == 0) tp4 = null;

            if (tp1 != null)
                   tp = tp1;          
            else if (tp2 != null) 
                  tp = tp2;
            else if (tp3 != null) 
                  tp = tp3;
            else if (tp4 != null) 
                  tp = tp4;
            if (dbg) System.out.printf("%sobject (%s -> %s)\n", tab(), nm, tp);
            if (tp == null) 
                  semantError(cc).printf("Undeclared identifier %s.\n", nm);
            else
                  ob.set_type(tp);
        }
        depth--;
    }

    /* Expression ->  */
    public void travers_ast(Expression ex) {
        class_ cc = (class_)smbt.probe(TreeConstants.curr_class);
        depth++;
        if (dbg) System.out.println(tab()+"Expression");
        String cn = ex.getClass().getName();
        if (dbg) System.out.printf("%sExpression (%s)\n", tab(), cn);
        if (cn.equals("block"))
	    travers_ast((block)ex); 
        else if (cn.equals("plus"))
	    travers_ast((plus)ex); 
        else if (cn.equals("sub"))
	    travers_ast((sub)ex); 
        else if (cn.equals("mul"))
	    travers_ast((mul)ex); 
        else if (cn.equals("divide"))
	    travers_ast((divide)ex); 
        else if (cn.equals("lt"))
	    travers_ast((lt)ex); 
        else if (cn.equals("eq"))
	    travers_ast((eq)ex); 
        else if (cn.equals("leq"))
	    travers_ast((leq)ex); 
        else if (cn.equals("neg"))
	    travers_ast((neg)ex); 
        else if (cn.equals("new_"))
	    travers_ast((new_)ex); 
        else if (cn.equals("comp"))
	    travers_ast((comp)ex); 
        else if (cn.equals("loop"))
	    travers_ast((loop)ex); 
        else if (cn.equals("cond"))
	    travers_ast((cond)ex); 
        else if (cn.equals("isvoid"))
	    travers_ast((isvoid)ex); 
        else if (cn.equals("int_const"))
	    travers_ast((int_const)ex); 
        else if (cn.equals("bool_const"))
	    travers_ast((bool_const)ex); 
        else if (cn.equals("string_const"))
	    travers_ast((string_const)ex); 
        else if (cn.equals("assign"))
	    travers_ast((assign)ex); 
        else if (cn.equals("dispatch"))
	    travers_ast((dispatch)ex); 
        else if (cn.equals("static_dispatch"))
	    travers_ast((static_dispatch)ex); 
        else if (cn.equals("let"))
	    travers_ast((let)ex); 
        else if (cn.equals("typcase"))
	    travers_ast((typcase)ex); 
        else if (cn.equals("object"))
	    travers_ast((object)ex); 
        else if (cn.equals("no_expr"))
	    travers_ast((no_expr)ex); 
        depth--;
    }

    /* block ->  */
    public void travers_ast(block bl) {
        class_ cc = (class_)smbt.probe(TreeConstants.curr_class);
        depth++;
        if (dbg) System.out.println(tab()+"block");
        Expression ex = bl;
        for (Enumeration e = bl.body.getElements(); e.hasMoreElements(); ) {
	    ex = (Expression)e.nextElement();
	    travers_ast(ex);
        }
        bl.set_type(ex.get_type());
        depth--;
    }

    /* plus ->  */
    public void travers_ast(plus pl) {
        class_ cc = (class_)smbt.probe(TreeConstants.curr_class);
        depth++;
        if (dbg) System.out.println(tab()+"plus");
        travers_ast(pl.e1);
        travers_ast(pl.e2);
        AbstractSymbol t1 = pl.e1.get_type();
        AbstractSymbol t2 = pl.e2.get_type();
        if (pass == 1) {
             if (t1.equals(TreeConstants.Int) && t2.equals(TreeConstants.Int)) {
                  pl.set_type(TreeConstants.Int);  
             }
             else {
                  semantError(cc).printf("non-Int arguments: %s + %s.\n", t1, t2);
             }
        }
        depth--;
    }

    /* sub ->  */
    public void travers_ast(sub mi) {
        class_ cc = (class_)smbt.probe(TreeConstants.curr_class);
        depth++;
        if (dbg) System.out.println(tab()+"sub");
        travers_ast(mi.e1);
        travers_ast(mi.e2);
        AbstractSymbol t1 = mi.e1.get_type();
        AbstractSymbol t2 = mi.e2.get_type();
        if (pass == 1) {
            if (t1.equals(TreeConstants.Int) && t2.equals(TreeConstants.Int)) {
                 mi.set_type(TreeConstants.Int);  
            }
            else {
                 semantError(cc).printf("non-Int arguments: %s + %s.\n", t1, t2);
            }
        }
        depth--;
    }

    /* mul ->  */
    public void travers_ast(mul mu) {
        class_ cc = (class_)smbt.probe(TreeConstants.curr_class);
        depth++;
        if (dbg) System.out.println(tab()+"mul");
        travers_ast(mu.e1);
        travers_ast(mu.e2);
        AbstractSymbol t1 = mu.e1.get_type();
        AbstractSymbol t2 = mu.e2.get_type();
        if (pass == 1) {
            if (t1.equals(TreeConstants.Int) && t2.equals(TreeConstants.Int)) {
                 mu.set_type(TreeConstants.Int);  
            }
            else {
                 semantError(cc).printf("non-Int arguments: %s + %s.\n", t1, t2);
            }
        }
        depth--;
    }

    /* divide ->  */
    public void travers_ast(divide di) {
        class_ cc = (class_)smbt.probe(TreeConstants.curr_class);
        depth++;
        if (dbg) System.out.println(tab()+"divide");
        travers_ast(di.e1);
        travers_ast(di.e2);
        AbstractSymbol t1 = di.e1.get_type();
        AbstractSymbol t2 = di.e2.get_type();
        if (pass == 1) {
            if (t1.equals(TreeConstants.Int) && t2.equals(TreeConstants.Int)) {
                 di.set_type(TreeConstants.Int);  
            }
            else {
                 semantError(cc).printf("non-Int arguments: %s + %s.\n", t1, t2);
            }
        }
        depth--;
    }

    /* lt ->  */
    public void travers_ast(lt x) {
        class_ cc = (class_)smbt.probe(TreeConstants.curr_class);
        depth++;
        if (dbg) System.out.println(tab()+"lt");
        travers_ast(x.e1);
        travers_ast(x.e2);
        AbstractSymbol t1 = x.e1.get_type();
        AbstractSymbol t2 = x.e2.get_type();
        x.set_type(TreeConstants.Bool);
        if (pass == 1 && !t1.equals(t2)) {
             boolean basic = false;
             if (dbg) System.out.printf("%scomparison eq types(%s,%s)\n", tab(), t1, t2);
             if (t1.equals(TreeConstants.Bool)) basic = true;
             if (t1.equals(TreeConstants.Str)) basic = true;
             if (t2.equals(TreeConstants.Bool)) basic = true;
             if (t2.equals(TreeConstants.Str)) basic = true;
             if (basic) semantError(cc).printf("Illegal comparison with a basic type.\n");
        }
        depth--;
    }

    /* eq ->  */
    public void travers_ast(eq x) {
        class_ cc = (class_)smbt.probe(TreeConstants.curr_class);
        depth++;
        if (dbg) System.out.println(tab()+"eq");
        travers_ast(x.e1);
        travers_ast(x.e2);
        AbstractSymbol t1 = x.e1.get_type();
        AbstractSymbol t2 = x.e2.get_type();
        x.set_type(TreeConstants.Bool);
        if (pass == 1 && !t1.equals(t2)) {
             boolean basic = false;
             if (dbg) System.out.printf("%scomparison eq types(%s,%s)\n", tab(), t1, t2);
             if (t1.equals(TreeConstants.Bool)) basic = true;
             if (t1.equals(TreeConstants.Str)) basic = true;
             if (t2.equals(TreeConstants.Bool)) basic = true;
             if (t2.equals(TreeConstants.Str)) basic = true;
             if (basic) semantError(cc).printf("Illegal comparison with a basic type.\n");
        }
        depth--;
    }

    /* leq ->  */
    public void travers_ast(leq x) {
        class_ cc = (class_)smbt.probe(TreeConstants.curr_class);
        depth++;
        if (dbg) System.out.println(tab()+"leq");
        travers_ast(x.e1);
        travers_ast(x.e2);
        AbstractSymbol t1 = x.e1.get_type();
        AbstractSymbol t2 = x.e2.get_type();
        x.set_type(TreeConstants.Bool);
        if (pass == 1 && !t1.equals(t2)) {
             boolean basic = false;
             if (dbg) System.out.printf("%scomparison eq types(%s,%s)\n", tab(), t1, t2);
             if (t1.equals(TreeConstants.Bool)) basic = true;
             if (t1.equals(TreeConstants.Str)) basic = true;
             if (t2.equals(TreeConstants.Bool)) basic = true;
             if (t2.equals(TreeConstants.Str)) basic = true;
             if (basic) semantError(cc).printf("Illegal comparison with a basic type.\n");
        }
        depth--;
    }

    /* neg ->  */
    public void travers_ast(neg x) {
        class_ cc = (class_)smbt.probe(TreeConstants.curr_class);
        depth++;
        if (dbg) System.out.println(tab()+"neg");
        travers_ast(x.e1);
        x.set_type(x.e1.get_type());
        depth--;
    }

    /* comp ->  */
    public void travers_ast(comp x) {
        class_ cc = (class_)smbt.probe(TreeConstants.curr_class);
        depth++;
        if (dbg) System.out.println(tab()+"comp");
        travers_ast(x.e1);
        if (pass == 1) {
            AbstractSymbol tp = x.e1.get_type();
            if (tp != null && tp.equals(TreeConstants.Bool)) {
                x.set_type(x.e1.get_type());
            }
            else {
                semantError(cc).printf("Argument of 'not' has type %s instead of Bool.\n", tp);
            } 
        }
        depth--;
    }

    /* cond ->  */
    public void travers_ast(cond x) {
        class_ cc = (class_)smbt.probe(TreeConstants.curr_class);
        depth++;
        if (dbg) System.out.println(tab()+"cond");
        travers_ast(x.pred);
        travers_ast(x.then_exp);
        travers_ast(x.else_exp);
        AbstractSymbol tp_pred = x.pred.get_type();
        if (pass == 1) {
            if (tp_pred.equals(TreeConstants.Bool)) {
                AbstractSymbol tp_then = x.then_exp.get_type();
                AbstractSymbol tp_else = x.else_exp.get_type();
                if (tp_else != null) x.set_type(lub(tp_then,tp_else));   
            }
            else {
                semantError(cc).printf("Predicate of 'if' does not have type Bool.\n");
            }
        }
        depth--;
    }

    /* loop ->  */
    public void travers_ast(loop x) {
        class_ cc = (class_)smbt.probe(TreeConstants.curr_class);
        depth++;
        if (dbg) System.out.println(tab()+"loop");
        travers_ast(x.pred);
        travers_ast(x.body);
        if (pass == 1) {
            AbstractSymbol tp_pred = x.pred.get_type();
            if (tp_pred.equals(TreeConstants.Bool)) {
                x.set_type(TreeConstants.Object_);
            }
            else {
                semantError(cc).printf("Loop condition does not have type Bool.\n");
            }
        }
        depth--;
    }

    /* isvoid ->  */
    public void travers_ast(isvoid x) {
        class_ cc = (class_)smbt.probe(TreeConstants.curr_class);
        depth++;
        if (dbg) System.out.println(tab()+"isvoid");
        travers_ast(x.e1);
        x.set_type(TreeConstants.Bool);
        depth--;
    }

    /* int_const ->  */
    public void travers_ast(int_const ic) {
        class_ cc = (class_)smbt.probe(TreeConstants.curr_class);
        depth++;
        if (dbg) System.out.println(tab()+"int_const");
        AbstractSymbol a = ic.token;
        ic.set_type(TreeConstants.Int);
        if (dbg) System.out.printf("%sint_const (%s)\n", tab(), a);
        depth--;
    }


    /* bool_const ->  */
    public void travers_ast(bool_const bc) {
        class_ cc = (class_)smbt.probe(TreeConstants.curr_class);
        depth++;
        if (dbg) System.out.println(tab()+"bool_const");
        boolean b = bc.val;
        bc.set_type(TreeConstants.Bool);
        if (dbg) System.out.printf("%sbool_const (%s)\n", tab(), b);
        depth--;
    }

    /* string_const ->  */
    public void travers_ast(string_const sc) {
        class_ cc = (class_)smbt.probe(TreeConstants.curr_class);
        depth++;
        if (dbg) System.out.println(tab()+"string_const");
        AbstractSymbol s = sc.token;
        sc.set_type(TreeConstants.Str);
        if (dbg) System.out.printf("%sstring_const (%s)\n", tab(), s);
        depth--;
    }

    /* let ->  */
    public void travers_ast(let x) {
        class_ cc = (class_)smbt.probe(TreeConstants.curr_class);
        method mm = (method)smbt.probe(TreeConstants.curr_method);
        smbt.enterScope();
        if (dbg) System.out.printf("%s***** ENTER SCOPE ****\n", tab());
        smbt.addId(TreeConstants.curr_class, cc);
        smbt.addId(TreeConstants.curr_method, mm);
        smbt.addId(TreeConstants.self, TreeConstants.SELF_TYPE);
        if (dbg) System.out.printf("%s***** SCOPE ADD %s ****\n", tab(), TreeConstants.curr_class);
        if (dbg) System.out.printf("%s***** SCOPE ADD %s ****\n", tab(), TreeConstants.curr_method);
        if (dbg) System.out.printf("%s***** SCOPE ADD %s ****\n", tab(), TreeConstants.self);
        depth++;
        let_level++;
        if (dbg) System.out.println(tab()+"let");
        AbstractSymbol id = x.identifier;
        if (x.identifier.equals(TreeConstants.self)) {
            if (pass == 1) semantError(cc).printf("'%s' cannot be bound in a 'let' expression.\n", id);
        }
        else {
             AbstractSymbol td = x.type_decl;
             if (test_type(td)) {
                 smbt.addId(id, td);
                 if (dbg) System.out.printf("%s***** SCOPE ADD %s ****\n", tab(), id);
                 if (dbg) System.out.printf("%slet (%s %s)\n", tab(), id, td);
                 Object[] env = new Object[2];
                 env[0] = td;
                 env[1] = cc;
                 String key = id.toString()+":"+cc.getName().toString()+":"+mm.name.toString()+":let";
                 att_map.put(key, env);
                 if (dbg) System.out.printf("%sattr att_map.put(key)\n", tab(), key);
             }
             else {
                 semantError(cc).printf("Class %s of let-bound identifier %s is undefined.\n", td, id);
             }
             travers_ast(x.init);
             assign xx = new assign(x.getLineNumber(), id, x.init);
             travers_ast(x.body);
             AbstractSymbol bt = x.body.get_type();
             AbstractSymbol it = x.init.get_type();
             if (pass == 1) {
                 AbstractSymbol lt = lub(td, it);
                 //if ((td.equals(TreeConstants.SELF_TYPE) && td.equals(it)) || (lt != null && lt.equals(td))) 
                 if (it == null || td.equals(it)) 
                      x.set_type(bt);
                 else
                     semantError(cc).printf("Inferred type %s of initialization of %s does not conform to identifier's declared type %s.\n", it, id, td);
             }
        }
        smbt.exitScope();
        if (dbg) System.out.printf("%s***** EXIT  SCOPE ****\n", tab());
        let_level--;
        depth--;
    }

    /* static_dispatch ->  */
    public void travers_ast(static_dispatch x) {
        class_ cc = (class_)smbt.probe(TreeConstants.curr_class);
        depth++;
        if (dbg) System.out.println(tab()+"static_dispatch");
        AbstractSymbol nm = x.name;
        AbstractSymbol tn = x.type_name;
        AbstractSymbol tp;
        travers_ast(x.actual);
        travers_ast(x.expr);
        if (!test_type(tn)) {
             semantError(cc).printf("Static dispatch to undefined class %s.\n", tn);
        }
        if (dbg) System.out.printf("%sstatic_dispatch (%s %s)\n", tab(), nm, tn);
        if (pass == 1) {
            AbstractSymbol ct = x.expr.get_type();
            AbstractSymbol lb = lub(ct, tn);
            if (dbg) System.out.printf("%sstatic_dispatch lub(expr,type) lub(%s,%s)=%s\n", tab(), ct, tn, lb);
            if (lb.equals(tn)) {
                if (dbg) System.out.printf("%sexpr type = %s\n", tab(), ct);
                if (dbg) System.out.printf("%sfind_mtd_map\n", tab());
                tp = find_mtd_map(x, x.type_name);
                if (tp == null)
                    semantError(cc).printf("Static dispatch to undefined method %s.\n", nm);
                else { 
                    if (tp.equals(TreeConstants.SELF_TYPE))
                        tp = ct;
                    x.set_type(tp);
                }
            }
            else {
                 semantError(cc).printf("Expression type %s does not conform to declared static dispatch type %s.\n", ct, tn);
            }
        }
        if (dbg) System.out.printf("%sStatic dispatch (%s)\n", tab(), nm);
        depth--;
    }

    /* dispatch ->  */
    public void travers_ast(dispatch x) {
        class_ cc = (class_)smbt.probe(TreeConstants.curr_class);
        depth++;
        if (dbg) System.out.println(tab()+"dispatch");
        AbstractSymbol nm = x.name;
        travers_ast(x.actual);
        travers_ast(x.expr);
        AbstractSymbol tp;
        if (pass == 1) {
            AbstractSymbol ct = x.expr.get_type();
            if (dbg) System.out.printf("%sexpr type = %s\n", tab(), ct);
            Object o = smbt.probe(nm); // try to find method in local scope
            if (o == null) {
                if (dbg) System.out.printf("%sfind_mtd_map (%s, %s)\n", tab(), nm, cc.getName());
                if (ct.equals(TreeConstants.SELF_TYPE))
                     tp = find_mtd_map(x, cc.getName());
                else
                     tp = find_mtd_map(x, ct);
                if (tp == null)
                    semantError(cc).printf("Dispatch to undefined method %s.\n", nm);
                else { 
                    if (tp.equals(TreeConstants.SELF_TYPE))
                        tp = ct;
                    x.set_type(tp);
                }
            }
            else {
                String cn = o.getClass().getName();
                if (cn.equals("method")) {
                    tp = ((method)o).return_type;
                    if (actual_formal(x.actual, (method)o))
                         x.set_type(tp);
                }
            }
        }
        if (dbg) System.out.printf("%sdispatch (%s)\n", tab(), nm);
        depth--;
    }

    /* branch ->  */
    public void travers_ast(branch x) {
        class_ cc = (class_)smbt.probe(TreeConstants.curr_class);
        depth++;
        if (dbg) System.out.println(tab()+"branch");
        AbstractSymbol nm = x.name;
        AbstractSymbol td = x.type_decl;  
        if (test_type(td)) {
            if (dbg) System.out.printf("%sbranch (%s %s)\n", tab(), nm, td);

            Object ox = smbt.probe(td);
            if (ox != null && ((String)ox).equals("__typedecl__")) {
                semantError(cc).printf("Duplicate branch %s in case statement.\n", td);
            }
            else {
                smbt.addId(td, "__typedecl__");
                if (dbg) System.out.printf("%s***** SCOPE ADD %s ****\n", tab(), td);
                smbt.enterScope();
                if (dbg) System.out.printf("%s***** ENTER SCOPE ****\n", tab());
                smbt.addId(nm, td);
                if (dbg) System.out.printf("%s***** SCOPE ADD %s ****\n", tab(), nm);
                travers_ast(x.expr);
                if (dbg) System.out.printf("%sbranch (%s %s) expression evaluated type is %s\n", tab(), nm, td, x.expr.get_type());
                smbt.exitScope();
                if (dbg) System.out.printf("%s***** EXIT  SCOPE ****\n", tab());
            }
        }
        else {
            semantError(cc).printf("Class %s of case branch is undefined.\n", td);
        }
        depth--;
    }

    /* Cases ->  */
    public void travers_ast(Cases x) {
        class_ cc = (class_)smbt.probe(TreeConstants.curr_class);
        depth++;
        if (dbg) System.out.println(tab()+"Cases");
        for (Enumeration e = x.getElements(); e.hasMoreElements(); ) {
	    travers_ast((branch)e.nextElement());
        }
        depth--;
    }

    /* typcase ->  */
    public void travers_ast(typcase x) {
        class_ cc = (class_)smbt.probe(TreeConstants.curr_class);
        smbt.enterScope();
        if (dbg) System.out.printf("%s***** ENTER SCOPE ****\n", tab());
        smbt.addId(TreeConstants.curr_class,cc);
        smbt.addId(TreeConstants.self, TreeConstants.SELF_TYPE);
        if (dbg) System.out.printf("%s***** SCOPE ADD %s ****\n", tab(), TreeConstants.curr_class);
        depth++;
        if (dbg) System.out.println(tab()+"typcase");
        travers_ast(x.expr);
        AbstractSymbol ct = x.expr.get_type();
        travers_ast(x.cases);
        if (pass == 1) {
            Enumeration e = x.cases.getElements();
            branch bb = (branch)e.nextElement();
            AbstractSymbol lt = bb.type_decl;
            AbstractSymbol lt1 = bb.expr.get_type();
            while (true) {
                if (!e.hasMoreElements()) break;
                bb = (branch)e.nextElement();
                lt = lub(lt, bb.type_decl);
                lt1 = lub(lt1, bb.expr.get_type());
                if (dbg) System.out.printf("%stypcase 1 lub(%s,%s)=%s\n", tab(), ct, bb.type_decl, lt);
                if (dbg) System.out.printf("%stypcase 2 lub(%s,%s)=%s\n", tab(), ct, bb.expr.get_type(), lt1);
            }
            if (dbg) System.out.printf("%stypcase 1 result type is %s\n", tab(), lt);
            if (dbg) System.out.printf("%stypcase 2 result type is %s\n", tab(), lt1);
            x.set_type(lt1);
        }
        depth--;
        smbt.exitScope();
        if (dbg) System.out.printf("%s***** EXIT  SCOPE ****\n", tab());
    }

    /* assign ->  */
    public void travers_ast(assign x) {
        class_ cc = (class_)smbt.probe(TreeConstants.curr_class);
        method mm = (method)smbt.probe(TreeConstants.curr_method);
        depth++;
        if (dbg) System.out.println(tab()+"assign");
        travers_ast(x.expr);
        AbstractSymbol nm = x.name;
        if (dbg) System.out.printf("%sassign (%s)\n", tab(), nm);
        if (nm.equals(TreeConstants.self)) {
              semantError(cc).printf("Cannot assign to '%s'.\n", nm);
        }
        else if (pass == 1) {
            AbstractSymbol tp = null;
            AbstractSymbol tp1 = null;
            AbstractSymbol tp2 = null;
            AbstractSymbol tp3 = null;
            AbstractSymbol tp4 = null;
            AbstractSymbol tpx = (x.expr).get_type();
            //Object o = smbt.probe(nm);
            //if (o != null) {
                //tp1 = (AbstractSymbol)o;
            //}
            //tp2 = find_att_map(nm, cc.getName());
            if (pass == 1) {
                Object o = smbt.probe(nm);
                if (o != null) {
                    if (dbg) System.out.printf("%sobject %s found in smbt\n", tab(), nm);
                    String cn = o.getClass().getName();
                    if (dbg) System.out.printf("%sobject class is %s \n", tab(), cn);
                    if (cn.equals("method")) {
                        tp1 = ((method)o).return_type;
                    }
                    else if (cn.equals("attr")) {
                        tp1 = ((attr)o).type_decl;
                    }
                    else if (cn.equals("java.lang.String")) {
                        //tp1 = TreeConstants.Str;
                        tp1 = null;
                    }
                    else {
                        tp1 = (AbstractSymbol) o;
                    }
                    if (tp1 != null) {
                        if (dbg) System.out.printf("%sobject type is %s, (return_type or type_decl)\n", tab(), tp1);
                    }
                }
                if (mm != null) {
                     String key = nm.toString()+":"+cc.getName().toString()+":"+mm.name.toString();
                     tp2 = find_fml_map(key);
                     if (dbg) System.out.printf("%sobject type is %s, (find_att_map 1)\n", tab(), tp2);
                }
                if (cc != null) {
                    tp3 = find_att_map(nm, cc.getName());
                    if (dbg) System.out.printf("%sobject type is %s, (find_att_map 2)\n", tab(), tp3);
                    if (mm != null) {
                        String key = nm.toString()+":"+cc.getName().toString()+":"+mm.name.toString()+":let";
                        tp4 = find_let_map(key);
                         if (dbg) System.out.printf("%sKEY %s object type is %s, (find_let_map 3)\n", tab(), key, tp4);
                    }
                }
            }
            if (let_level == 0) tp4 = null;

            if (tp1 != null) 
                tp = tp1;
            else if (tp2 != null) 
                tp = tp2;
            else if (tp3 != null) 
                tp = tp3;
            else if (tp4 != null) 
                tp = tp4;
            else
                semantError(cc).printf("Assignment to undeclared variable %s.\n", nm);
                     
            if (tp != null) {
                 if (dbg) System.out.printf("%sassign tp =%s=\n", tab(), tp);
                 if (dbg) System.out.printf("%sassign tpx =%s=\n", tab(), tpx);
                 AbstractSymbol lt = lub(tp, tpx);
                 if (lt.equals(tp)) {
                      x.set_type(tp);
                 }
                 else {
                     semantError(cc).printf("Type %s of assigned expression does not conform to declared type %s of identifier %s.\n", tpx, tp, nm);
                 }
            }
        }
        depth--;
    }

    /* new_ ->  */
    public void travers_ast(new_ x) {
        class_ cc = (class_)smbt.probe(TreeConstants.curr_class);
        depth++;
        if (dbg) System.out.println(tab()+"new_");
        AbstractSymbol tn = x.type_name;
        if (test_type(tn)) {
             x.set_type(tn);
        }
        else {
             semantError(cc).printf("'new' used with undefined class %s.\n", tn);
        }
        if (dbg) System.out.printf("%snew_ (%s)\n", tab(), tn);
        depth--;
    }

    /* no_expr ->  */
    public void travers_ast(no_expr ne) {
        class_ cc = (class_)smbt.probe(TreeConstants.curr_class);
        depth++;
        if (dbg) System.out.println(tab()+"no_expr");
        depth--;
    }
/* ======================================================================== */

    public ClassTable(Classes cls) {
        depth = 0;
	semantErrors = 0;
	errorStream = System.err;
        cls_map = new HashMap<AbstractSymbol, class_> ();
        mtd_map = new HashMap<String, Object[]>();
        att_map = new HashMap<String, Object[]>();
        CgenClassTable.current_cls_map = cls_map;
        CgenClassTable.current_att_map = att_map;
        CgenClassTable.current_mtd_map = mtd_map;
	char[] bytes = new char[100];
	Arrays.fill(bytes, ' ');
	blank_line = new String(bytes);
	
	/* fill this in */
        dbg = false;
        let_level = 0;
        smbt = new SymbolTable();

	/* Installs basic classes Object_class, IO_class, Int_class,
           Bool_class, and Str_class */

        installBasicClasses();

        class_ x, y, z;
        for (int i = 0; i < cls.getLength(); i++) {
            x = (class_)cls.getNth(i);
            if (cls_map.get(x.getName()) != null) {
                semantError(x).printf("Class %s was previously defined.\n", x.getName());
                break;
            }
            if ((x.getName()).equals(TreeConstants.SELF_TYPE)) {
                semantError(x).printf("Redefinition of basic class %s.\n", x.getName());
                break;
            }
            if (pass == 0) cls_map.put(x.getName(), x);
        }

        if (semantErrors == 0) {
            HashMap<AbstractSymbol,class_> mc = test_missing();
            for (AbstractSymbol key: mc.keySet()) {
                class_ cc = mc.get(key);
                semantError(mc.get(key)).printf("Class %s inherits from an undefined class %s.\n", key.toString(), cc.getParent().toString());
            }
        }

        if (semantErrors == 0) {
            HashMap<AbstractSymbol,class_> bc = test_basic();
            for (AbstractSymbol key: bc.keySet()) {
                class_ cc = bc.get(key);
                semantError(bc.get(key)).printf("Class %s cannot inherit class %s.\n", key.toString(), cc.getParent().toString());
            }
        }

        if (semantErrors == 0) {
            HashSet<AbstractSymbol> lx = test_loop();
            for (AbstractSymbol key: lx) {
                 semantError(cls_map.get(key)).printf("Class %s, or an ancestor of %s, is involved in an inheritance cycle.\n", key.toString(), key.toString());
            }
        }

        if (semantErrors == 0) {
            if (!test_Main()) {
                 semantError().printf("Class Main is not defined.\n");
            }
        }

        if (semantErrors == 0 && false) {
            if (dbg) System.out.printf("############################\n");
            for (int i = 0; i < cls.getLength(); i++) {
                x = (class_)cls.getNth(i);
                for (int j = 0; j < cls.getLength(); j++) {
                    y = (class_)cls.getNth(j);
                    System.out.printf("lub(%s,%s)=", x.getName(), y.getName());
                    System.out.printf("%s\n", lub(x.getName(), y.getName()) );
                }
            }
            if (dbg) System.out.printf("############################\n");
        }
        
        if (semantErrors == 0) {
            pass = 0;
            if (dbg) System.out.printf("############################\n");
            if (dbg) System.out.printf("####### travers_ast %d #####\n", pass);
            travers_ast(cls);
            if (dbg) list_mtd_map();
            if (dbg) list_att_map();
            if (dbg) System.out.printf("####### travers_ast %d #####\n", pass);
            if (dbg) System.out.printf("############################\n");
            pass = 1;
            if (dbg) System.out.printf("############################\n");
            if (dbg) System.out.printf("####### travers_ast %d #####\n", pass);
            travers_ast(cls);
            if (dbg) System.out.printf("####### travers_ast %d #####\n", pass);
            if (dbg) System.out.printf("############################\n");
        }
    }

    /** Prints line number and file name of the given class.
     *
     * Also increments semantic error count.
     *
     * @param c the class
     * @return a print stream to which the rest of the error message is
     * to be printed.
     *
     * */
    public PrintStream semantError(class_ c) {
	return semantError(c.getFilename(), c);
    }

    /** Prints the file name and the line number of the given tree node.
     *
     * Also increments semantic error count.
     *
     * @param filename the file name
     * @param t the tree node
     * @return a print stream to which the rest of the error message is
     * to be printed.
     *
     * */
    public PrintStream semantError(AbstractSymbol filename, TreeNode t) {
	errorStream.print(filename + ":" + t.getLineNumber() + ": ");
	return semantError();
    }

    /** Increments semantic error count and returns the print stream for
     * error messages.
     *
     * @return a print stream to which the error message is
     * to be printed.
     *
     * */
    public PrintStream semantError() {
	semantErrors++;
	return errorStream;
    }

    /** Returns true if there are any static semantic errors. */
    public boolean errors() {
	return semantErrors != 0;
    }
}
			  
    
