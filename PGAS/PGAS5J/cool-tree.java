// -*- mode: java -*- 
//
// file: cool-tree.m4
//
// This file defines the AST
//
//////////////////////////////////////////////////////////



import java.util.Enumeration;
import java.io.PrintStream;
import java.util.Vector;
import java.util.Stack;


/** Defines simple phylum Program */
abstract class Program extends TreeNode {
    protected Program(int lineNumber) {
        super(lineNumber);
    }
    public abstract void dump_with_types(PrintStream out, int n);
    public abstract void semant();
    public abstract void cgen(PrintStream s);

}


/** Defines simple phylum Class_ */
abstract class Class_ extends TreeNode {
    protected Class_(int lineNumber) {
        super(lineNumber);
    }
    public abstract void dump_with_types(PrintStream out, int n);
    public abstract AbstractSymbol getName();
    public abstract AbstractSymbol getParent();
    public abstract AbstractSymbol getFilename();
    public abstract Features getFeatures();

}


/** Defines list phylum Classes
    <p>
    See <a href="ListNode.html">ListNode</a> for full documentation. */
class Classes extends ListNode {
    public final static Class elementClass = Class_.class;
    /** Returns class of this lists's elements */
    public Class getElementClass() {
        return elementClass;
    }
    protected Classes(int lineNumber, Vector elements) {
        super(lineNumber, elements);
    }
    /** Creates an empty "Classes" list */
    public Classes(int lineNumber) {
        super(lineNumber);
    }
    /** Appends "Class_" element to this list */
    public Classes appendElement(TreeNode elem) {
        addElement(elem);
        return this;
    }
    public TreeNode copy() {
        return new Classes(lineNumber, copyElements());
    }
}


/** Defines simple phylum Feature */
abstract class Feature extends TreeNode {
    protected Feature(int lineNumber) {
        super(lineNumber);
    }
    public abstract void dump_with_types(PrintStream out, int n);

}


/** Defines list phylum Features
    <p>
    See <a href="ListNode.html">ListNode</a> for full documentation. */
class Features extends ListNode {
    public final static Class elementClass = Feature.class;
    /** Returns class of this lists's elements */
    public Class getElementClass() {
        return elementClass;
    }
    protected Features(int lineNumber, Vector elements) {
        super(lineNumber, elements);
    }
    /** Creates an empty "Features" list */
    public Features(int lineNumber) {
        super(lineNumber);
    }
    /** Appends "Feature" element to this list */
    public Features appendElement(TreeNode elem) {
        addElement(elem);
        return this;
    }
    public TreeNode copy() {
        return new Features(lineNumber, copyElements());
    }
}


/** Defines simple phylum Formal */
abstract class Formal extends TreeNode {
    protected Formal(int lineNumber) {
        super(lineNumber);
    }
    public abstract void dump_with_types(PrintStream out, int n);

}


/** Defines list phylum Formals
    <p>
    See <a href="ListNode.html">ListNode</a> for full documentation. */
class Formals extends ListNode {
    public final static Class elementClass = Formal.class;
    /** Returns class of this lists's elements */
    public Class getElementClass() {
        return elementClass;
    }
    protected Formals(int lineNumber, Vector elements) {
        super(lineNumber, elements);
    }
    /** Creates an empty "Formals" list */
    public Formals(int lineNumber) {
        super(lineNumber);
    }
    /** Appends "Formal" element to this list */
    public Formals appendElement(TreeNode elem) {
        addElement(elem);
        return this;
    }
    public TreeNode copy() {
        return new Formals(lineNumber, copyElements());
    }
}


/** Defines simple phylum Expression */
abstract class Expression extends TreeNode {
    protected Expression(int lineNumber) {
        super(lineNumber);
    }
    private AbstractSymbol type = null;                                 
    public AbstractSymbol get_type() { return type; }           
    public Expression set_type(AbstractSymbol s) { type = s; return this; } 
    public abstract void dump_with_types(PrintStream out, int n);
    public void dump_type(PrintStream out, int n) {
        if (type != null)
            { out.println(Utilities.pad(n) + ": " + type.getString()); }
        else
            { out.println(Utilities.pad(n) + ": _no_type"); }
    }
    public abstract void code(PrintStream s);

}


/** Defines list phylum Expressions
    <p>
    See <a href="ListNode.html">ListNode</a> for full documentation. */
class Expressions extends ListNode {
    public final static Class elementClass = Expression.class;
    /** Returns class of this lists's elements */
    public Class getElementClass() {
        return elementClass;
    }
    protected Expressions(int lineNumber, Vector elements) {
        super(lineNumber, elements);
    }
    /** Creates an empty "Expressions" list */
    public Expressions(int lineNumber) {
        super(lineNumber);
    }
    /** Appends "Expression" element to this list */
    public Expressions appendElement(TreeNode elem) {
        addElement(elem);
        return this;
    }
    public TreeNode copy() {
        return new Expressions(lineNumber, copyElements());
    }
}


/** Defines simple phylum Case */
abstract class Case extends TreeNode {
    protected Case(int lineNumber) {
        super(lineNumber);
    }
    public abstract void dump_with_types(PrintStream out, int n);

}


/** Defines list phylum Cases
    <p>
    See <a href="ListNode.html">ListNode</a> for full documentation. */
class Cases extends ListNode {
    public final static Class elementClass = Case.class;
    /** Returns class of this lists's elements */
    public Class getElementClass() {
        return elementClass;
    }
    protected Cases(int lineNumber, Vector elements) {
        super(lineNumber, elements);
    }
    /** Creates an empty "Cases" list */
    public Cases(int lineNumber) {
        super(lineNumber);
    }
    /** Appends "Case" element to this list */
    public Cases appendElement(TreeNode elem) {
        addElement(elem);
        return this;
    }
    public TreeNode copy() {
        return new Cases(lineNumber, copyElements());
    }
}


/** Defines AST constructor 'program'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class program extends Program {
    public Classes classes;
    /** Creates "program" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for classes
      */
    public program(int lineNumber, Classes a1) {
        super(lineNumber);
        classes = a1;
    }
    public TreeNode copy() {
        return new program(lineNumber, (Classes)classes.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "program\n");
        classes.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_program");
        for (Enumeration e = classes.getElements(); e.hasMoreElements(); ) {
	    ((Class_)e.nextElement()).dump_with_types(out, n + 1);
        }
    }
    /** This method is the entry point to the semantic checker.  You will
        need to complete it in programming assignment 4.
	<p>
        Your checker should do the following two things:
	<ol>
	<li>Check that the program is semantically correct
	<li>Decorate the abstract syntax tree with type information
        by setting the type field in each Expression node.
        (see tree.h)
	</ol>
	<p>
	You are free to first do (1) and make sure you catch all semantic
    	errors. Part (2) can be done in a second stage when you want
	to test the complete compiler.
    */
    public void semant() {
	/* ClassTable constructor may do some semantic analysis */
	ClassTable classTable = new ClassTable(classes);
	
	/* some semantic analysis code may go here */

	if (classTable.errors()) {
	    System.err.println("Compilation halted due to static semantic errors.");
	    System.exit(1);
	}
    }
    /** This method is the entry point to the code generator.  All of the work
      * of the code generator takes place within CgenClassTable constructor.
      * @param s the output stream 
      * @see CgenClassTable
      * */
    public void cgen(PrintStream s) 
    {
        // spim wants comments to start with '#'
        s.print("# start of generated code\n");

	CgenClassTable codegen_classtable = new CgenClassTable(classes, s);

	s.print("\n# end of generated code\n");
    }

}


/** Defines AST constructor 'class_'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class class_ extends Class_ {
    public AbstractSymbol name;
    public AbstractSymbol parent;
    public Features features;
    public AbstractSymbol filename;
    /** Creates "class_" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for name
      * @param a1 initial value for parent
      * @param a2 initial value for features
      * @param a3 initial value for filename
      */
    public class_(int lineNumber, AbstractSymbol a1, AbstractSymbol a2, Features a3, AbstractSymbol a4) {
        super(lineNumber);
        name = a1;
        parent = a2;
        features = a3;
        filename = a4;
    }
    public TreeNode copy() {
        return new class_(lineNumber, copy_AbstractSymbol(name), copy_AbstractSymbol(parent), (Features)features.copy(), copy_AbstractSymbol(filename));
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "class_\n");
        dump_AbstractSymbol(out, n+2, name);
        dump_AbstractSymbol(out, n+2, parent);
        features.dump(out, n+2);
        dump_AbstractSymbol(out, n+2, filename);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_class");
        dump_AbstractSymbol(out, n + 2, name);
        dump_AbstractSymbol(out, n + 2, parent);
        out.print(Utilities.pad(n + 2) + "\"");
        Utilities.printEscapedString(out, filename.getString());
        out.println("\"\n" + Utilities.pad(n + 2) + "(");
        for (Enumeration e = features.getElements(); e.hasMoreElements();) {
	    ((Feature)e.nextElement()).dump_with_types(out, n + 2);
        }
        out.println(Utilities.pad(n + 2) + ")");
    }
    public AbstractSymbol getName()     { return name; }
    public AbstractSymbol getParent()   { return parent; }
    public AbstractSymbol getFilename() { return filename; }
    public Features getFeatures()       { return features; }

}


/** Defines AST constructor 'method'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class method extends Feature {
    public AbstractSymbol name;
    public Formals formals;
    public AbstractSymbol return_type;
    public Expression expr;
    /** Creates "method" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for name
      * @param a1 initial value for formals
      * @param a2 initial value for return_type
      * @param a3 initial value for expr
      */
    public method(int lineNumber, AbstractSymbol a1, Formals a2, AbstractSymbol a3, Expression a4) {
        super(lineNumber);
        name = a1;
        formals = a2;
        return_type = a3;
        expr = a4;
    }
    public TreeNode copy() {
        return new method(lineNumber, copy_AbstractSymbol(name), (Formals)formals.copy(), copy_AbstractSymbol(return_type), (Expression)expr.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "method\n");
        dump_AbstractSymbol(out, n+2, name);
        formals.dump(out, n+2);
        dump_AbstractSymbol(out, n+2, return_type);
        expr.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_method");
        dump_AbstractSymbol(out, n + 2, name);
        for (Enumeration e = formals.getElements(); e.hasMoreElements();) {
	    ((Formal)e.nextElement()).dump_with_types(out, n + 2);
        }
        dump_AbstractSymbol(out, n + 2, return_type);
	expr.dump_with_types(out, n + 2);
    }

}


/** Defines AST constructor 'attr'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class attr extends Feature {
    public AbstractSymbol name;
    public AbstractSymbol type_decl;
    public Expression init;
    /** Creates "attr" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for name
      * @param a1 initial value for type_decl
      * @param a2 initial value for init
      */
    public attr(int lineNumber, AbstractSymbol a1, AbstractSymbol a2, Expression a3) {
        super(lineNumber);
        name = a1;
        type_decl = a2;
        init = a3;
    }
    public TreeNode copy() {
        return new attr(lineNumber, copy_AbstractSymbol(name), copy_AbstractSymbol(type_decl), (Expression)init.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "attr\n");
        dump_AbstractSymbol(out, n+2, name);
        dump_AbstractSymbol(out, n+2, type_decl);
        init.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_attr");
        dump_AbstractSymbol(out, n + 2, name);
        dump_AbstractSymbol(out, n + 2, type_decl);
	init.dump_with_types(out, n + 2);
    }

}


/** Defines AST constructor 'formal'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class formal extends Formal {
    public AbstractSymbol name;
    public AbstractSymbol type_decl;
    /** Creates "formal" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for name
      * @param a1 initial value for type_decl
      */
    public formal(int lineNumber, AbstractSymbol a1, AbstractSymbol a2) {
        super(lineNumber);
        name = a1;
        type_decl = a2;
    }
    public TreeNode copy() {
        return new formal(lineNumber, copy_AbstractSymbol(name), copy_AbstractSymbol(type_decl));
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "formal\n");
        dump_AbstractSymbol(out, n+2, name);
        dump_AbstractSymbol(out, n+2, type_decl);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_formal");
        dump_AbstractSymbol(out, n + 2, name);
        dump_AbstractSymbol(out, n + 2, type_decl);
    }

}


/** Defines AST constructor 'branch'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class branch extends Case {
    public AbstractSymbol name;
    public AbstractSymbol type_decl;
    public Expression expr;
    /** Creates "branch" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for name
      * @param a1 initial value for type_decl
      * @param a2 initial value for expr
      */
    public branch(int lineNumber, AbstractSymbol a1, AbstractSymbol a2, Expression a3) {
        super(lineNumber);
        name = a1;
        type_decl = a2;
        expr = a3;
    }
    public TreeNode copy() {
        return new branch(lineNumber, copy_AbstractSymbol(name), copy_AbstractSymbol(type_decl), (Expression)expr.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "branch\n");
        dump_AbstractSymbol(out, n+2, name);
        dump_AbstractSymbol(out, n+2, type_decl);
        expr.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_branch");
        dump_AbstractSymbol(out, n + 2, name);
        dump_AbstractSymbol(out, n + 2, type_decl);
	expr.dump_with_types(out, n + 2);
    }

}


/** Defines AST constructor 'assign'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class assign extends Expression {
    public AbstractSymbol name;
    public Expression expr;
    /** Creates "assign" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for name
      * @param a1 initial value for expr
      */
    public assign(int lineNumber, AbstractSymbol a1, Expression a2) {
        super(lineNumber);
        name = a1;
        expr = a2;
    }
    public TreeNode copy() {
        return new assign(lineNumber, copy_AbstractSymbol(name), (Expression)expr.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "assign\n");
        dump_AbstractSymbol(out, n+2, name);
        expr.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_assign");
        dump_AbstractSymbol(out, n + 2, name);
	expr.dump_with_types(out, n + 2);
	dump_type(out, n);
    }
    /** Generates code for this expression.  This method is to be completed 
      * in programming assignment 5.  (You may add or remove parameters as
      * you wish.)
      * @param s the output stream 
      * */
    public void code(PrintStream s) {
        if (Flags.cgen_debug) System.out.printf("####### assign ENTER #####\n");
         if (expr != null) expr.code(s);
         int mode = 0;
         CgenNode cn = CgenClassTable.current_cgen_node;
         dispatch cd = CgenClassTable.current_dispatch;
         String cc = CgenClassTable.current_caller;
         
         Integer ioff = null;
         if (CgenClassTable.obj_map != null && cn != null) {
             if (Flags.cgen_debug) System.out.printf("#QUERY %s\n",cn.getName()+":"+cc+":"+name);
             ioff = CgenClassTable.obj_map.get(cn.getName()+":"+cc+":"+name);
             if (ioff == null) {
                 if (Flags.cgen_debug) System.out.printf("#QUERY %s\n",cn.getName()+":"+name);
                 ioff = CgenClassTable.obj_map.get(cn.getName()+":"+name);
                 mode = 1;
             }
         }
         int off = 3;
         if (ioff != null) off = ioff.intValue();
         if (mode == 0) 
             CgenSupport.emitStore(CgenSupport.ACC, off, CgenSupport.FP, s);
         else if (mode == 1)
             CgenSupport.emitStore(CgenSupport.ACC, off, CgenSupport.SELF, s);


        if (Flags.cgen_debug) System.out.printf("####### assign EXIT #####\n");
    }


}


/** Defines AST constructor 'static_dispatch'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class static_dispatch extends Expression {
    public Expression expr;
    public AbstractSymbol type_name;
    public AbstractSymbol name;
    public Expressions actual;
    /** Creates "static_dispatch" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for expr
      * @param a1 initial value for type_name
      * @param a2 initial value for name
      * @param a3 initial value for actual
      */
    public static_dispatch(int lineNumber, Expression a1, AbstractSymbol a2, AbstractSymbol a3, Expressions a4) {
        super(lineNumber);
        expr = a1;
        type_name = a2;
        name = a3;
        actual = a4;
    }
    public TreeNode copy() {
        return new static_dispatch(lineNumber, (Expression)expr.copy(), copy_AbstractSymbol(type_name), copy_AbstractSymbol(name), (Expressions)actual.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "static_dispatch\n");
        expr.dump(out, n+2);
        dump_AbstractSymbol(out, n+2, type_name);
        dump_AbstractSymbol(out, n+2, name);
        actual.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_static_dispatch");
	expr.dump_with_types(out, n + 2);
        dump_AbstractSymbol(out, n + 2, type_name);
        dump_AbstractSymbol(out, n + 2, name);
        out.println(Utilities.pad(n + 2) + "(");
        for (Enumeration e = actual.getElements(); e.hasMoreElements();) {
	    ((Expression)e.nextElement()).dump_with_types(out, n + 2);
        }
        out.println(Utilities.pad(n + 2) + ")");
	dump_type(out, n);
    }
    /** Generates code for this expression.  This method is to be completed 
      * in programming assignment 5.  (You may add or remove parameters as
      * you wish.)
      * @param s the output stream 
      * */
    public void code(PrintStream s) {
        if (Flags.cgen_debug) System.out.printf("####### static_dispatch ENTER #####\n");
        if (Flags.cgen_debug) System.out.printf("####### static_dispatch EXIT #####\n");
    }


}


/** Defines AST constructor 'dispatch'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class dispatch extends Expression {
    public Expression expr;
    public AbstractSymbol name;
    public Expressions actual;
    /** Creates "dispatch" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for expr
      * @param a1 initial value for name
      * @param a2 initial value for actual
      */
    public dispatch(int lineNumber, Expression a1, AbstractSymbol a2, Expressions a3) {
        super(lineNumber);
        expr = a1;
        name = a2;
        actual = a3;
    }
    public TreeNode copy() {
        return new dispatch(lineNumber, (Expression)expr.copy(), copy_AbstractSymbol(name), (Expressions)actual.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "dispatch\n");
        expr.dump(out, n+2);
        dump_AbstractSymbol(out, n+2, name);
        actual.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_dispatch");
	expr.dump_with_types(out, n + 2);
        dump_AbstractSymbol(out, n + 2, name);
        out.println(Utilities.pad(n + 2) + "(");
        for (Enumeration e = actual.getElements(); e.hasMoreElements();) {
	    ((Expression)e.nextElement()).dump_with_types(out, n + 2);
        }
        out.println(Utilities.pad(n + 2) + ")");
	dump_type(out, n);
    }
    /** Generates code for this expression.  This method is to be completed 
      * in programming assignment 5.  (You may add or remove parameters as
      * you wish.)
      * @param s the output stream 
      * */
    public void code(PrintStream s) {
        if (Flags.cgen_debug) System.out.printf("####### dispatch ENTER #####\n");
        if (Flags.cgen_debug) System.out.printf("#=========== DISPATCH %s ==== START\n",
                  name.toString());
        CgenNode x = null;
        AbstractSymbol et = expr.get_type();
        if (et.equals(TreeConstants.SELF_TYPE))
            x = CgenClassTable.current_cgen_node;
        else {
            x = (CgenNode)CgenClassTable.symtab.probe(et);
            if (Flags.cgen_debug) System.out.printf("#DISPATCH %s class %s\n",
                  name.toString(), x.getName().toString());
        }
        String cn;
        method mx = null;
        //=============================================================================
        //-----------------------------------------------------
        // lw      $t1 16($t1)
        // IO_dispTab:
        //      .word   Object.abort             OFFSET  0*4
        //      .word   Object.type_name                 1*4
        //      .word   Object.copy                      2*4
        //      .word   IO.out_string                    3*4
        //      .word   IO.out_int                       4*4
        //      .word   IO.in_string                     5*4
        //      .word   IO.in_int                        6*4
        //-----------------------------------------------------
        Stack<CgenNode> sn = new Stack<CgenNode>();
        int cnt1 = 0;
        boolean found = false;
        while (true) {
             if (x == null) break;
             if ((x.getName()).equals(TreeConstants.No_class)) break;
             sn.push(x);
             x = x.getParentNd();
        }
        while (true) {
             if (sn.empty()) break;
             x = sn.pop();
             Features fts = x.getFeatures();
             for (Enumeration e = fts.getElements(); e.hasMoreElements(); ) {
                 Object o = e.nextElement();
                 String sn1 = o.getClass().getName();
                 if (sn1.equals("method")) {
                     if ((((method)o).name).equals(name)) {
                        found = true;
                        mx = (method) o;
                     }
                     cnt1++;
                 }
                 if (found) break;
            }
            if (found) break;
        }

        //=============================================================================
        if (et != null && et.equals(TreeConstants.SELF_TYPE)) {
             CgenSupport.emitMove(CgenSupport.S1, CgenSupport.SELF, s);
        }
        else {
             expr.code(s);
             CgenSupport.emitMove(CgenSupport.S1, CgenSupport.ACC, s);
        }
        int base = 3;
        CgenSupport.emitStore(CgenSupport.S1, 0, CgenSupport.SP, s);
        CgenSupport.emitAddiu(CgenSupport.SP, CgenSupport.SP, -4, s);
        //=============================================================================

        CgenClassTable.label_cnt++;
        int cnt = CgenClassTable.label_cnt;
        int i = actual.getLength();
        if (mx != null && mx.formals != null) {
            Enumeration ef = mx.formals.getElements();
            for (Enumeration e = actual.getElements(); e.hasMoreElements(); ) {
                Object o = e.nextElement();
                if (ef != null && ef.hasMoreElements()) {
                    formal fc = (formal)ef.nextElement();
                    cn = o.getClass().getName();
                if (Flags.cgen_debug) System.out.printf("#node %s dispatch %s -> actual %s offset %d -> %s\n", 
                       x.getName(), name, fc.name, base+i, cn);
                    CgenClassTable.obj_map.remove(x.getName()+":"+name+":"+fc.name);
                    CgenClassTable.obj_map.put(x.getName()+":"+name+":"+fc.name, base+i--);
                }
                // code "object"
                ((Expression) o).code(s);
                // sw      $a0 0($sp)
                CgenSupport.emitStore(CgenSupport.ACC, 0, CgenSupport.SP, s);
                // addiu   $sp $sp -4
                CgenSupport.emitAddiu(CgenSupport.SP, CgenSupport.SP, -4, s);
            }
        }
        //=============================================================================
        // move    $a0 $s0
        if (Flags.cgen_debug) System.out.printf("#----- self --- START\n");
        CgenSupport.emitLoad(CgenSupport.ACC, 4, CgenSupport.SP, s);
        if (Flags.cgen_debug) System.out.printf("#----- self --- END\n");
        //=============================================================================
        // bne     $a0 $zero label0
        CgenSupport.emitBne(CgenSupport.ACC, CgenSupport.ZERO, cnt, s);
        // la      $a0 str_const0   (source file name)
        CgenSupport.emitLoadAddress(CgenSupport.ACC, "str_const0", s);
        // li      $t1 1
        CgenSupport.emitLoadImm(CgenSupport.T1, 1, s);
        // jal     _dispatch_abort
        CgenSupport.emitJal("_dispatch_abort", s);
        // label0:
        CgenSupport.emitLabelDef(cnt, s);
        // lw      $t1 8($a0)   DISPATCH POINTER
        CgenSupport.emitLoad(CgenSupport.T1, 2, CgenSupport.ACC, s);
        CgenSupport.emitLoad(CgenSupport.T1, cnt1-1, CgenSupport.T1, s);
        // jalr            $t1
        CgenSupport.emitJalr(CgenSupport.T1, s);
        // pop self entry
        CgenSupport.emitAddiu(CgenSupport.SP, CgenSupport.SP, 4, s);

        CgenClassTable.current_dispatch = this;
        if (Flags.cgen_debug) System.out.printf("#=========== DISPATCH %s ==== END\n",
                  name.toString());
        if (Flags.cgen_debug) System.out.printf("####### dispatch EXIT #####\n");
    }


}


/** Defines AST constructor 'cond'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class cond extends Expression {
    public Expression pred;
    public Expression then_exp;
    public Expression else_exp;
    /** Creates "cond" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for pred
      * @param a1 initial value for then_exp
      * @param a2 initial value for else_exp
      */
    public cond(int lineNumber, Expression a1, Expression a2, Expression a3) {
        super(lineNumber);
        pred = a1;
        then_exp = a2;
        else_exp = a3;
    }
    public TreeNode copy() {
        return new cond(lineNumber, (Expression)pred.copy(), (Expression)then_exp.copy(), (Expression)else_exp.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "cond\n");
        pred.dump(out, n+2);
        then_exp.dump(out, n+2);
        else_exp.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_cond");
	pred.dump_with_types(out, n + 2);
	then_exp.dump_with_types(out, n + 2);
	else_exp.dump_with_types(out, n + 2);
	dump_type(out, n);
    }
    /** Generates code for this expression.  This method is to be completed 
      * in programming assignment 5.  (You may add or remove parameters as
      * you wish.)
      * @param s the output stream 
      * */
    public void code(PrintStream s) {
        if (Flags.cgen_debug) System.out.printf("####### cond ENTER #####\n");
        CgenClassTable.label_cnt += 2;
        int cnt = CgenClassTable.label_cnt;
        pred.code(s);
	CgenSupport.emitLoadBool(CgenSupport.S1, new BoolConst(true), s);
        CgenSupport.emitBeq(CgenSupport.S1, CgenSupport.ACC, cnt-1, s);
        else_exp.code(s); 
        CgenSupport.emitBranch(cnt, s);
        CgenSupport.emitLabelDef(cnt-1, s);
        then_exp.code(s); 
        CgenSupport.emitLabelDef(cnt, s);
        if (Flags.cgen_debug) System.out.printf("####### cond EXIT #####\n");
    }


}


/** Defines AST constructor 'loop'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class loop extends Expression {
    public Expression pred;
    public Expression body;
    /** Creates "loop" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for pred
      * @param a1 initial value for body
      */
    public loop(int lineNumber, Expression a1, Expression a2) {
        super(lineNumber);
        pred = a1;
        body = a2;
    }
    public TreeNode copy() {
        return new loop(lineNumber, (Expression)pred.copy(), (Expression)body.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "loop\n");
        pred.dump(out, n+2);
        body.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_loop");
	pred.dump_with_types(out, n + 2);
	body.dump_with_types(out, n + 2);
	dump_type(out, n);
    }
    /** Generates code for this expression.  This method is to be completed 
      * in programming assignment 5.  (You may add or remove parameters as
      * you wish.)
      * @param s the output stream 
      * */
    public void code(PrintStream s) {
        if (Flags.cgen_debug) System.out.printf("####### loop ENTER #####\n");
        if (Flags.cgen_debug) System.out.printf("####### loop EXIT #####\n");
    }


}


/** Defines AST constructor 'typcase'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class typcase extends Expression {
    public Expression expr;
    public Cases cases;
    /** Creates "typcase" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for expr
      * @param a1 initial value for cases
      */
    public typcase(int lineNumber, Expression a1, Cases a2) {
        super(lineNumber);
        expr = a1;
        cases = a2;
    }
    public TreeNode copy() {
        return new typcase(lineNumber, (Expression)expr.copy(), (Cases)cases.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "typcase\n");
        expr.dump(out, n+2);
        cases.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_typcase");
	expr.dump_with_types(out, n + 2);
        for (Enumeration e = cases.getElements(); e.hasMoreElements();) {
	    ((Case)e.nextElement()).dump_with_types(out, n + 2);
        }
	dump_type(out, n);
    }
    /** Generates code for this expression.  This method is to be completed 
      * in programming assignment 5.  (You may add or remove parameters as
      * you wish.)
      * @param s the output stream 
      * */
    public void code(PrintStream s) {
        if (Flags.cgen_debug) System.out.printf("####### typcase ENTER #####\n");
        if (Flags.cgen_debug) System.out.printf("####### typcase EXIT #####\n");
    }


}


/** Defines AST constructor 'block'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class block extends Expression {
    public Expressions body;
    /** Creates "block" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for body
      */
    public block(int lineNumber, Expressions a1) {
        super(lineNumber);
        body = a1;
    }
    public TreeNode copy() {
        return new block(lineNumber, (Expressions)body.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "block\n");
        body.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_block");
        for (Enumeration e = body.getElements(); e.hasMoreElements();) {
	    ((Expression)e.nextElement()).dump_with_types(out, n + 2);
        }
	dump_type(out, n);
    }
    /** Generates code for this expression.  This method is to be completed 
      * in programming assignment 5.  (You may add or remove parameters as
      * you wish.)
      * @param s the output stream 
      * */
    public void code(PrintStream s) {
        if (Flags.cgen_debug) System.out.printf("####### block ENTER #####\n");
        Expression ex = null;
        for (Enumeration e = body.getElements(); e.hasMoreElements(); ) {
            ex = (Expression)e.nextElement();
            ex.code(s);
        }
        if (Flags.cgen_debug) System.out.printf("####### block EXIT #####\n");
    }


}


/** Defines AST constructor 'let'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class let extends Expression {
    public AbstractSymbol identifier;
    public AbstractSymbol type_decl;
    public Expression init;
    public Expression body;
    /** Creates "let" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for identifier
      * @param a1 initial value for type_decl
      * @param a2 initial value for init
      * @param a3 initial value for body
      */
    public let(int lineNumber, AbstractSymbol a1, AbstractSymbol a2, Expression a3, Expression a4) {
        super(lineNumber);
        identifier = a1;
        type_decl = a2;
        init = a3;
        body = a4;
    }
    public TreeNode copy() {
        return new let(lineNumber, copy_AbstractSymbol(identifier), copy_AbstractSymbol(type_decl), (Expression)init.copy(), (Expression)body.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "let\n");
        dump_AbstractSymbol(out, n+2, identifier);
        dump_AbstractSymbol(out, n+2, type_decl);
        init.dump(out, n+2);
        body.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_let");
	dump_AbstractSymbol(out, n + 2, identifier);
	dump_AbstractSymbol(out, n + 2, type_decl);
	init.dump_with_types(out, n + 2);
	body.dump_with_types(out, n + 2);
	dump_type(out, n);
    }
    /** Generates code for this expression.  This method is to be completed 
      * in programming assignment 5.  (You may add or remove parameters as
      * you wish.)
      * @param s the output stream 
      * */
    public void code(PrintStream s) {
        if (Flags.cgen_debug) System.out.printf("####### let ENTER ##### (%s)\n", identifier);
        AbstractSymbol xx1 = AbstractTable.inttable.lookup("0");
        AbstractSymbol xx2 = AbstractTable.stringtable.lookup("");

        CgenNode x = CgenClassTable.current_cgen_node;
        String cc = CgenClassTable.current_caller;
        CgenClassTable.let_count++;
        CgenClassTable.let_max_count = CgenClassTable.let_count;
        if (Flags.cgen_debug) System.out.printf("#----- LET DEF --- START (%s)\n", identifier);
        if (type_decl.equals(TreeConstants.Int)) {
            CgenSupport.emitPartialLoadAddress(CgenSupport.ACC, s);
            ((IntSymbol)xx1).codeRef(s);
            s.println("");
        }
        if (type_decl.equals(TreeConstants.Bool)) {
            CgenSupport.emitPartialLoadAddress(CgenSupport.ACC, s);
            BoolConst.falsebool.codeRef(s);
            s.println("");
        }
        if (type_decl.equals(TreeConstants.Str)) {
            CgenSupport.emitPartialLoadAddress(CgenSupport.ACC, s);
            ((StringSymbol)xx2).codeRef(s);
            s.println("");
        }
        if (Flags.cgen_debug) System.out.printf("#----- LET DEF --- END (%s)\n", identifier);
   
        if (Flags.cgen_debug) System.out.printf("#INSERT (LET) KEY %s VAL %d (%s)\n",  
        x.getName()+":"+cc+":let:"+CgenClassTable.let_count+":"+identifier, CgenClassTable.let_count, identifier);
        CgenClassTable.obj_map.remove(x.getName()+":"+cc+":let:"+CgenClassTable.let_count+":"+identifier);
        CgenClassTable.obj_map.put(x.getName()+":"+cc+":let:"+CgenClassTable.let_count+":"+identifier, 
            CgenClassTable.let_count);
        if (Flags.cgen_debug) System.out.printf("#----- LET INIT --- START (%s)\n", identifier);
        init.code(s);
        // sw      $a0 0($sp)
        CgenSupport.emitStore(CgenSupport.ACC, 0, CgenSupport.SP, s);
        // addiu   $sp $sp -4
        CgenSupport.emitAddiu(CgenSupport.SP, CgenSupport.SP, -4, s);
        if (Flags.cgen_debug) System.out.printf("#----- LET INIT --- END (%s)\n", identifier);
        if (Flags.cgen_debug) System.out.printf("#----- LET BODY --- START (%s)\n", identifier);
        body.code(s);
        if (Flags.cgen_debug) System.out.printf("#----- LET BODY --- END (%s)\n", identifier);
        CgenClassTable.let_count--;
        if (CgenClassTable.let_count == 0) {
           // addiu $sp $sp 12  ---- ON EXIT FROM NESTED LET !!!
            CgenSupport.emitAddiu(CgenSupport.SP, CgenSupport.SP, 
                                  4*CgenClassTable.let_max_count, s);
            CgenClassTable.let_max_count = 0;
        }
        if (Flags.cgen_debug) System.out.printf("####### let EXIT ##### (%s)\n", identifier);
    }


}


/** Defines AST constructor 'plus'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class plus extends Expression {
    public Expression e1;
    public Expression e2;
    /** Creates "plus" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for e1
      * @param a1 initial value for e2
      */
    public plus(int lineNumber, Expression a1, Expression a2) {
        super(lineNumber);
        e1 = a1;
        e2 = a2;
    }
    public TreeNode copy() {
        return new plus(lineNumber, (Expression)e1.copy(), (Expression)e2.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "plus\n");
        e1.dump(out, n+2);
        e2.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_plus");
	e1.dump_with_types(out, n + 2);
	e2.dump_with_types(out, n + 2);
	dump_type(out, n);
    }
    /** Generates code for this expression.  This method is to be completed 
      * in programming assignment 5.  (You may add or remove parameters as
      * you wish.)
      * @param s the output stream 
      * */
    public void code(PrintStream s) {
        if (Flags.cgen_debug) System.out.printf("####### plus ENTER #####\n");
        e1.code(s);         // e1 result object is now in $a0
        //Location p( $a0 );   // copy pointer to $a0 somewhere safe, 
                             // either the stack or one of the $s1-$s6 registers
        CgenSupport.emitMove(CgenSupport.S1, CgenSupport.ACC, s);
        e2.code(s);         // e2 result object is now in $a0
	//jal	Object.copy ------------- $a0 is now a temporary copy of e2
        CgenSupport.emitJal("Object.copy", s);
        //p.Load( $t1 );       // e1 is now in $t1 -- note that we waited until 
                             // after e2 evaluated to do this, because we can't 
                             // count on e2 to restore the contents of $t1
        //load( $t2, 3, $a0 ); // load the value of e2 into $t2
        CgenSupport.emitLoad(CgenSupport.T2, 3, CgenSupport.ACC, s);
        //load( $t1, 3, $t1 ); // load the value of e1 into $t1
        CgenSupport.emitLoad(CgenSupport.T1, 3, CgenSupport.S1, s);
        // add     $t1 $t1 $t2
        CgenSupport.emitAdd(CgenSupport.T1, CgenSupport.T1, CgenSupport.T2, s);
        // sw      $t1 12($a0)
        CgenSupport.emitStore(CgenSupport.T1, 3, CgenSupport.ACC, s);
        if (Flags.cgen_debug) System.out.printf("####### plus EXIT #####\n");
    }

}


/** Defines AST constructor 'sub'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class sub extends Expression {
    public Expression e1;
    public Expression e2;
    /** Creates "sub" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for e1
      * @param a1 initial value for e2
      */
    public sub(int lineNumber, Expression a1, Expression a2) {
        super(lineNumber);
        e1 = a1;
        e2 = a2;
    }
    public TreeNode copy() {
        return new sub(lineNumber, (Expression)e1.copy(), (Expression)e2.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "sub\n");
        e1.dump(out, n+2);
        e2.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_sub");
	e1.dump_with_types(out, n + 2);
	e2.dump_with_types(out, n + 2);
	dump_type(out, n);
    }
    /** Generates code for this expression.  This method is to be completed 
      * in programming assignment 5.  (You may add or remove parameters as
      * you wish.)
      * @param s the output stream 
      * */
    public void code(PrintStream s) {
        if (Flags.cgen_debug) System.out.printf("####### sub ENTER #####\n");
        e1.code(s);
        CgenSupport.emitMove(CgenSupport.S1, CgenSupport.ACC, s);
        e2.code(s); 
        CgenSupport.emitJal("Object.copy", s);
        CgenSupport.emitLoad(CgenSupport.T2, 3, CgenSupport.ACC, s);
        CgenSupport.emitLoad(CgenSupport.T1, 3, CgenSupport.S1, s);
        CgenSupport.emitSub(CgenSupport.T1, CgenSupport.T1, CgenSupport.T2, s);
        CgenSupport.emitStore(CgenSupport.T1, 3, CgenSupport.ACC, s);
        if (Flags.cgen_debug) System.out.printf("####### sub EXIT #####\n");
    }


}


/** Defines AST constructor 'mul'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class mul extends Expression {
    public Expression e1;
    public Expression e2;
    /** Creates "mul" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for e1
      * @param a1 initial value for e2
      */
    public mul(int lineNumber, Expression a1, Expression a2) {
        super(lineNumber);
        e1 = a1;
        e2 = a2;
    }
    public TreeNode copy() {
        return new mul(lineNumber, (Expression)e1.copy(), (Expression)e2.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "mul\n");
        e1.dump(out, n+2);
        e2.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_mul");
	e1.dump_with_types(out, n + 2);
	e2.dump_with_types(out, n + 2);
	dump_type(out, n);
    }
    /** Generates code for this expression.  This method is to be completed 
      * in programming assignment 5.  (You may add or remove parameters as
      * you wish.)
      * @param s the output stream 
      * */
    public void code(PrintStream s) {
        if (Flags.cgen_debug) System.out.printf("####### mul ENTER #####\n");
        e1.code(s);
        CgenSupport.emitMove(CgenSupport.S1, CgenSupport.ACC, s);
        e2.code(s); 
        CgenSupport.emitJal("Object.copy", s);
        CgenSupport.emitLoad(CgenSupport.T2, 3, CgenSupport.ACC, s);
        CgenSupport.emitLoad(CgenSupport.T1, 3, CgenSupport.S1, s);
        CgenSupport.emitMul(CgenSupport.T1, CgenSupport.T1, CgenSupport.T2, s);
        CgenSupport.emitStore(CgenSupport.T1, 3, CgenSupport.ACC, s);
        if (Flags.cgen_debug) System.out.printf("####### mul EXIT #####\n");
    }


}


/** Defines AST constructor 'divide'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class divide extends Expression {
    public Expression e1;
    public Expression e2;
    /** Creates "divide" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for e1
      * @param a1 initial value for e2
      */
    public divide(int lineNumber, Expression a1, Expression a2) {
        super(lineNumber);
        e1 = a1;
        e2 = a2;
    }
    public TreeNode copy() {
        return new divide(lineNumber, (Expression)e1.copy(), (Expression)e2.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "divide\n");
        e1.dump(out, n+2);
        e2.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_divide");
	e1.dump_with_types(out, n + 2);
	e2.dump_with_types(out, n + 2);
	dump_type(out, n);
    }
    /** Generates code for this expression.  This method is to be completed 
      * in programming assignment 5.  (You may add or remove parameters as
      * you wish.)
      * @param s the output stream 
      * */
    public void code(PrintStream s) {
        if (Flags.cgen_debug) System.out.printf("####### divide ENTER #####\n");
        e1.code(s);
        CgenSupport.emitMove(CgenSupport.S1, CgenSupport.ACC, s);
        e2.code(s); 
        CgenSupport.emitJal("Object.copy", s);
        CgenSupport.emitLoad(CgenSupport.T2, 3, CgenSupport.ACC, s);
        CgenSupport.emitLoad(CgenSupport.T1, 3, CgenSupport.S1, s);
        CgenSupport.emitDiv(CgenSupport.T1, CgenSupport.T1, CgenSupport.T2, s);
        CgenSupport.emitStore(CgenSupport.T1, 3, CgenSupport.ACC, s);
        if (Flags.cgen_debug) System.out.printf("####### divide EXIT #####\n");
    }


}


/** Defines AST constructor 'neg'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class neg extends Expression {
    public Expression e1;
    /** Creates "neg" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for e1
      */
    public neg(int lineNumber, Expression a1) {
        super(lineNumber);
        e1 = a1;
    }
    public TreeNode copy() {
        return new neg(lineNumber, (Expression)e1.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "neg\n");
        e1.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_neg");
	e1.dump_with_types(out, n + 2);
	dump_type(out, n);
    }
    /** Generates code for this expression.  This method is to be completed 
      * in programming assignment 5.  (You may add or remove parameters as
      * you wish.)
      * @param s the output stream 
      * */
    public void code(PrintStream s) {
        if (Flags.cgen_debug) System.out.printf("####### neg ENTER #####\n");
	// la	$a0 int_const0
        e1.code(s);
	// jal	Object.copy
        CgenSupport.emitJal("Object.copy", s);
	// lw	$t1 12($a0)
        CgenSupport.emitLoad(CgenSupport.T1, 3, CgenSupport.ACC, s);
	// neg	$t1 $t1
        CgenSupport.emitNeg(CgenSupport.T1, CgenSupport.T1, s);
	// sw	$t1 12($a0)
        CgenSupport.emitStore(CgenSupport.T1, 3, CgenSupport.ACC, s);
        if (Flags.cgen_debug) System.out.printf("####### neg EXIT #####\n");
    }


}


/** Defines AST constructor 'lt'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class lt extends Expression {
    public Expression e1;
    public Expression e2;
    /** Creates "lt" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for e1
      * @param a1 initial value for e2
      */
    public lt(int lineNumber, Expression a1, Expression a2) {
        super(lineNumber);
        e1 = a1;
        e2 = a2;
    }
    public TreeNode copy() {
        return new lt(lineNumber, (Expression)e1.copy(), (Expression)e2.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "lt\n");
        e1.dump(out, n+2);
        e2.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_lt");
	e1.dump_with_types(out, n + 2);
	e2.dump_with_types(out, n + 2);
	dump_type(out, n);
    }
    /** Generates code for this expression.  This method is to be completed 
      * in programming assignment 5.  (You may add or remove parameters as
      * you wish.)
      * @param s the output stream 
      * */
    public void code(PrintStream s) {
        if (Flags.cgen_debug) System.out.printf("####### lt ENTER #####\n");
        e1.code(s);
        CgenSupport.emitMove(CgenSupport.T1, CgenSupport.ACC, s);
        e2.code(s); 
        CgenSupport.emitMove(CgenSupport.T2, CgenSupport.ACC, s);
        CgenClassTable.label_cnt++;
        int cnt = CgenClassTable.label_cnt;
	CgenSupport.emitLoadBool(CgenSupport.ACC, new BoolConst(true), s);
        CgenSupport.emitBlt(CgenSupport.T1, CgenSupport.T2, cnt, s);
	CgenSupport.emitLoadBool(CgenSupport.ACC, new BoolConst(false), s);
        CgenSupport.emitLabelDef(cnt, s);
        if (Flags.cgen_debug) System.out.printf("####### lt EXIT #####\n");
    }


}


/** Defines AST constructor 'eq'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class eq extends Expression {
    public Expression e1;
    public Expression e2;
    /** Creates "eq" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for e1
      * @param a1 initial value for e2
      */
    public eq(int lineNumber, Expression a1, Expression a2) {
        super(lineNumber);
        e1 = a1;
        e2 = a2;
    }
    public TreeNode copy() {
        return new eq(lineNumber, (Expression)e1.copy(), (Expression)e2.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "eq\n");
        e1.dump(out, n+2);
        e2.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_eq");
	e1.dump_with_types(out, n + 2);
	e2.dump_with_types(out, n + 2);
	dump_type(out, n);
    }
    /** Generates code for this expression.  This method is to be completed 
      * in programming assignment 5.  (You may add or remove parameters as
      * you wish.)
      * @param s the output stream 
      * */
    public void code(PrintStream s) {
        if (Flags.cgen_debug) System.out.printf("####### eq ENTER #####\n");
        e1.code(s);
        CgenSupport.emitMove(CgenSupport.T1, CgenSupport.ACC, s);
        e2.code(s); 
        CgenSupport.emitMove(CgenSupport.T2, CgenSupport.ACC, s);
        CgenClassTable.label_cnt++;
        int cnt = CgenClassTable.label_cnt;
	CgenSupport.emitLoadBool(CgenSupport.ACC, new BoolConst(true), s);
        CgenSupport.emitBeq(CgenSupport.T1, CgenSupport.T2, cnt, s);
	CgenSupport.emitLoadBool(CgenSupport.A1, new BoolConst(false), s);
        CgenSupport.emitJal("equality_test", s);
        CgenSupport.emitLabelDef(cnt, s);
        if (Flags.cgen_debug) System.out.printf("####### eq EXIT #####\n");
    }
}


/** Defines AST constructor 'leq'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class leq extends Expression {
    public Expression e1;
    public Expression e2;
    /** Creates "leq" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for e1
      * @param a1 initial value for e2
      */
    public leq(int lineNumber, Expression a1, Expression a2) {
        super(lineNumber);
        e1 = a1;
        e2 = a2;
    }
    public TreeNode copy() {
        return new leq(lineNumber, (Expression)e1.copy(), (Expression)e2.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "leq\n");
        e1.dump(out, n+2);
        e2.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_leq");
	e1.dump_with_types(out, n + 2);
	e2.dump_with_types(out, n + 2);
	dump_type(out, n);
    }
    /** Generates code for this expression.  This method is to be completed 
      * in programming assignment 5.  (You may add or remove parameters as
      * you wish.)
      * @param s the output stream 
      * */
    public void code(PrintStream s) {
        if (Flags.cgen_debug) System.out.printf("####### leq ENTER #####\n");
        e1.code(s);
        CgenSupport.emitMove(CgenSupport.T1, CgenSupport.ACC, s);
        e2.code(s); 
        CgenSupport.emitMove(CgenSupport.T2, CgenSupport.ACC, s);
        CgenClassTable.label_cnt++;
        int cnt = CgenClassTable.label_cnt;
	CgenSupport.emitLoadBool(CgenSupport.ACC, new BoolConst(true), s);
        CgenSupport.emitBleq(CgenSupport.T1, CgenSupport.T2, cnt, s);
	CgenSupport.emitLoadBool(CgenSupport.ACC, new BoolConst(false), s);
        CgenSupport.emitLabelDef(cnt, s);
        if (Flags.cgen_debug) System.out.printf("####### leq EXIT #####\n");
    }


}


/** Defines AST constructor 'comp'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class comp extends Expression {
    public Expression e1;
    /** Creates "comp" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for e1
      */
    public comp(int lineNumber, Expression a1) {
        super(lineNumber);
        e1 = a1;
    }
    public TreeNode copy() {
        return new comp(lineNumber, (Expression)e1.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "comp\n");
        e1.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_comp");
	e1.dump_with_types(out, n + 2);
	dump_type(out, n);
    }
    /** Generates code for this expression.  This method is to be completed 
      * in programming assignment 5.  (You may add or remove parameters as
      * you wish.)
      * @param s the output stream 
      * */
    public void code(PrintStream s) {
        if (Flags.cgen_debug) System.out.printf("####### comp ENTER #####\n");
        CgenClassTable.label_cnt++;
        int cnt = CgenClassTable.label_cnt;
	// la      $a0 bool_const1
        e1.code(s);
	// lw	$t1 12($a0)
        CgenSupport.emitMove(CgenSupport.T1, CgenSupport.ACC, s);
        //CgenSupport.emitLoad(CgenSupport.T1, 3, CgenSupport.ACC, s);


	// la	$a0 bool_const1
	CgenSupport.emitLoadBool(CgenSupport.ACC, new BoolConst(false), s);
	// beqz	$t1 label0
        CgenSupport.emitBne(CgenSupport.T1, CgenSupport.ACC, cnt, s);
	// la	$a0 bool_const0
	CgenSupport.emitLoadBool(CgenSupport.ACC, new BoolConst(true), s);
        // label0:
        CgenSupport.emitLabelDef(cnt, s);


	// la	$a0 bool_const1
	//CgenSupport.emitLoadBool(CgenSupport.ACC, new BoolConst(true), s);
	// beqz	$t1 label0
        //CgenSupport.emitBeqz(CgenSupport.T1, cnt, s);
	// la	$a0 bool_const0
	//CgenSupport.emitLoadBool(CgenSupport.ACC, new BoolConst(false), s);
        // label0:
        //CgenSupport.emitLabelDef(cnt, s);
        if (Flags.cgen_debug) System.out.printf("####### comp EXIT #####\n");
    }


}


/** Defines AST constructor 'int_const'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class int_const extends Expression {
    public AbstractSymbol token;
    /** Creates "int_const" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for token
      */
    public int_const(int lineNumber, AbstractSymbol a1) {
        super(lineNumber);
        token = a1;
    }
    public TreeNode copy() {
        return new int_const(lineNumber, copy_AbstractSymbol(token));
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "int_const\n");
        dump_AbstractSymbol(out, n+2, token);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_int");
	dump_AbstractSymbol(out, n + 2, token);
	dump_type(out, n);
    }
    /** Generates code for this expression.  This method method is provided
      * to you as an example of code generation.
      * @param s the output stream 
      * */
    public void code(PrintStream s) {
        if (Flags.cgen_debug) System.out.printf("####### int_const ENTER #####\n");
	CgenSupport.emitLoadInt(CgenSupport.ACC,
                                (IntSymbol)AbstractTable.inttable.lookup(token.getString()), s);
        if (Flags.cgen_debug) System.out.printf("####### int_const EXIT #####\n");
    }

}


/** Defines AST constructor 'bool_const'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class bool_const extends Expression {
    public Boolean val;
    /** Creates "bool_const" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for val
      */
    public bool_const(int lineNumber, Boolean a1) {
        super(lineNumber);
        val = a1;
    }
    public TreeNode copy() {
        return new bool_const(lineNumber, copy_Boolean(val));
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "bool_const\n");
        dump_Boolean(out, n+2, val);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_bool");
	dump_Boolean(out, n + 2, val);
	dump_type(out, n);
    }
    /** Generates code for this expression.  This method method is provided
      * to you as an example of code generation.
      * @param s the output stream 
      * */
    public void code(PrintStream s) {
        if (Flags.cgen_debug) System.out.printf("####### bool_const ENTER #####\n");
	CgenSupport.emitLoadBool(CgenSupport.ACC, new BoolConst(val), s);
        if (Flags.cgen_debug) System.out.printf("####### bool_const EXIT #####\n");
    }

}


/** Defines AST constructor 'string_const'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class string_const extends Expression {
    public AbstractSymbol token;
    /** Creates "string_const" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for token
      */
    public string_const(int lineNumber, AbstractSymbol a1) {
        super(lineNumber);
        token = a1;
    }
    public TreeNode copy() {
        return new string_const(lineNumber, copy_AbstractSymbol(token));
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "string_const\n");
        dump_AbstractSymbol(out, n+2, token);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_string");
	out.print(Utilities.pad(n + 2) + "\"");
	Utilities.printEscapedString(out, token.getString());
	out.println("\"");
	dump_type(out, n);
    }
    /** Generates code for this expression.  This method method is provided
      * to you as an example of code generation.
      * @param s the output stream 
      * */
    public void code(PrintStream s) {
        if (Flags.cgen_debug) System.out.printf("####### string_const ENTER #####\n");
	CgenSupport.emitLoadString(CgenSupport.ACC,
                                   (StringSymbol)AbstractTable.stringtable.lookup(token.getString()), s);
        if (Flags.cgen_debug) System.out.printf("####### string_const EXIT #####\n");
    }

}


/** Defines AST constructor 'new_'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class new_ extends Expression {
    public AbstractSymbol type_name;
    /** Creates "new_" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for type_name
      */
    public new_(int lineNumber, AbstractSymbol a1) {
        super(lineNumber);
        type_name = a1;
    }
    public TreeNode copy() {
        return new new_(lineNumber, copy_AbstractSymbol(type_name));
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "new_\n");
        dump_AbstractSymbol(out, n+2, type_name);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_new");
	dump_AbstractSymbol(out, n + 2, type_name);
	dump_type(out, n);
    }
    /** Generates code for this expression.  This method is to be completed 
      * in programming assignment 5.  (You may add or remove parameters as
      * you wish.)
      * @param s the output stream 
      * */
    public void code(PrintStream s) {
        if (Flags.cgen_debug) System.out.printf("####### _new ENTER #####\n");
	//la	$a0 ABC_protObj
        CgenSupport.emitLoadAddress(CgenSupport.ACC, 
                type_name.toString()+CgenSupport.PROTOBJ_SUFFIX, s);
	//jal	Object.copy
        CgenSupport.emitJal("Object.copy", s);
	//jal	ABC_init
        CgenSupport.emitJal(type_name.toString()+CgenSupport.CLASSINIT_SUFFIX, s);
        if (Flags.cgen_debug) System.out.printf("####### _new EXIT #####\n");
    }


}


/** Defines AST constructor 'isvoid'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class isvoid extends Expression {
    public Expression e1;
    /** Creates "isvoid" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for e1
      */
    public isvoid(int lineNumber, Expression a1) {
        super(lineNumber);
        e1 = a1;
    }
    public TreeNode copy() {
        return new isvoid(lineNumber, (Expression)e1.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "isvoid\n");
        e1.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_isvoid");
	e1.dump_with_types(out, n + 2);
	dump_type(out, n);
    }
    /** Generates code for this expression.  This method is to be completed 
      * in programming assignment 5.  (You may add or remove parameters as
      * you wish.)
      * @param s the output stream 
      * */
    public void code(PrintStream s) {
        if (Flags.cgen_debug) System.out.printf("####### isvoid ENTER #####\n");
        CgenClassTable.label_cnt++;
        int cnt = CgenClassTable.label_cnt;
	// la	$a0 int_const0
        e1.code(s);
	// move	$t1 $a0
        CgenSupport.emitMove(CgenSupport.T1, CgenSupport.ACC, s);
	// la	$a0 bool_const1
	CgenSupport.emitLoadBool(CgenSupport.ACC, new BoolConst(true), s);
	// beqz	$t1 label0
        CgenSupport.emitBeqz(CgenSupport.T1, cnt, s);
	// la	$a0 bool_const0
	CgenSupport.emitLoadBool(CgenSupport.ACC, new BoolConst(false), s);
        // label0:
        CgenSupport.emitLabelDef(cnt, s);
        if (Flags.cgen_debug) System.out.printf("####### isvoid EXIT #####\n");
    }


}


/** Defines AST constructor 'no_expr'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class no_expr extends Expression {
    /** Creates "no_expr" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      */
    public no_expr(int lineNumber) {
        super(lineNumber);
    }
    public TreeNode copy() {
        return new no_expr(lineNumber);
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "no_expr\n");
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_no_expr");
	dump_type(out, n);
    }
    /** Generates code for this expression.  This method is to be completed 
      * in programming assignment 5.  (You may add or remove parameters as
      * you wish.)
      * @param s the output stream 
      * */
    public void code(PrintStream s) {
        if (Flags.cgen_debug) System.out.printf("####### no_expr ENTER #####\n");
        if (Flags.cgen_debug) System.out.printf("####### no_expr EXIT #####\n");
    }


}


/** Defines AST constructor 'object'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class object extends Expression {
    public AbstractSymbol name;
    /** Creates "object" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for name
      */
    public object(int lineNumber, AbstractSymbol a1) {
        super(lineNumber);
        name = a1;
    }
    public TreeNode copy() {
        return new object(lineNumber, copy_AbstractSymbol(name));
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "object\n");
        dump_AbstractSymbol(out, n+2, name);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_object");
	dump_AbstractSymbol(out, n + 2, name);
	dump_type(out, n);
    }
    /** Generates code for this expression.  This method is to be completed 
      * in programming assignment 5.  (You may add or remove parameters as
      * you wish.)
      * @param s the output stream 
      * */
    public void code(PrintStream s) {
          if (Flags.cgen_debug) System.out.printf("####### object ENTER #####\n");
         int mode = 0;
         CgenNode cn = CgenClassTable.current_cgen_node;
         dispatch cd = CgenClassTable.current_dispatch;
         String cc = CgenClassTable.current_caller;
         
         Integer ioff = null;
         CgenClassTable.list_obj_map();
         if (CgenClassTable.obj_map != null && cn != null) {
             for (int i=CgenClassTable.let_count; i>=1; i--) {
                ioff = CgenClassTable.obj_map.get(cn.getName()+":"+cc+":let:"+i+":"+name);
                if (Flags.cgen_debug) System.out.printf("#QUERY %s RESULT %d\n",cn.getName()+":"+cc+":let:"+i+":"+name, (ioff != null) ? ioff.intValue() : -1);
                mode = 2;
                if (ioff != null) break;
             }
             if (ioff == null) {
                 ioff = CgenClassTable.obj_map.get(cn.getName()+":"+cc+":"+name);
                 if (Flags.cgen_debug) System.out.printf("#QUERY %s RESULT %d\n", cn.getName()+":"+cc+":"+name, (ioff != null) ? ioff.intValue() : -1);
                 if (ioff == null) {
                     ioff = CgenClassTable.obj_map.get(cn.getName()+":"+name);
                     if (Flags.cgen_debug) System.out.printf("#QUERY %s RESULT %d\n", cn.getName()+":"+name, (ioff != null) ? ioff.intValue() : -1);
                     if (ioff != null)  mode = 1;
                 }
             }
         }
         int off = 3;
         if (ioff != null) off = ioff.intValue();
         if (mode == 0) 
             CgenSupport.emitLoad(CgenSupport.ACC, off, CgenSupport.FP, s);
         else if (mode == 1)
             CgenSupport.emitLoad(CgenSupport.ACC, off+2, CgenSupport.SELF, s);
         else if (mode == 2) {
             CgenSupport.emitLoad(CgenSupport.ACC, 
                        CgenClassTable.let_max_count - off + 1, CgenSupport.SP, s);
         }
         if (Flags.cgen_debug) System.out.printf("####### object EXIT #####\n");
    }

}


