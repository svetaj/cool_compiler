/*
 *  The scanner definition for COOL.
 */

import java_cup.runtime.Symbol;


%%


%{

/*  Stuff enclosed in %{ %} is copied verbatim to the lexer class
 *  definition, all the extra variables/functions you want to use in the
 *  lexer actions should go here.  Don't remove or modify anything that
 *  was there initially.  */

    // Max size of string constants
    static int MAX_STR_CONST = 1025;

    // For assembling string constants
    StringBuffer string_buf = new StringBuffer();

    private int curr_lineno = 1;
    int get_curr_lineno() {
        curr_lineno = yyline+1;
	return curr_lineno;
    }

    private AbstractSymbol filename;

    void set_filename(String fname) {
	filename = AbstractTable.stringtable.addString(fname);
    }

    AbstractSymbol curr_filename() {
	return filename;
    }

    int commentDepth = 0;
    boolean star = false;
    boolean lparen = false;
    boolean escquote = false;
    boolean escnull = false;
    boolean singlenull = false;
    StringBuilder stringtext = new StringBuilder("");
%}

%init{

/*  Stuff enclosed in %init{ %init} is copied verbatim to the lexer
 *  class constructor, all the extra initialization you want to do should
 *  go here.  Don't remove or modify anything that was there initially. */

    // empty for now
%init}

%eofval{

/*  Stuff enclosed in %eofval{ %eofval} specifies java code that is
 *  executed when end-of-file is reached.  If you use multiple lexical
 *  states and want to do something special if an EOF is encountered in
 *  one of those states, place your code in the switch statement.
 *  Ultimately, you should return the EOF symbol, or your lexer won't
 *  work.  */

    switch(yy_lexical_state) {
    case YYINITIAL:
	/* nothing special to do in the initial state */
	break;
    case COMMENT:
        yybegin(YYINITIAL);
        return new Symbol(TokenConstants.ERROR, "EOF in comment");
    case STRING:
        yybegin(YYINITIAL);
        escquote = false;
        stringtext = new StringBuilder("");
        return new Symbol(TokenConstants.ERROR, "EOF in string constant");
    }
    return new Symbol(TokenConstants.EOF);
%eofval}


%class CoolLexer
%cup

%line

%state COMMENT,STRING


%%

<YYINITIAL> "(*"     { yybegin(COMMENT); commentDepth++; }
<COMMENT> "("           { if (lparen == false) 
                              lparen = true;  
                          else
                              lparen = false;
                        }
<COMMENT> "*"           { 
                            if (lparen == true) {
                                commentDepth++;
                                lparen = false;
                            }
                            else
                                star = true;  
                        }
<COMMENT> ")"              {     if (star == true) {
                                    star = false;
				    commentDepth--; 
				    if (commentDepth == 0) {
				  	yybegin(YYINITIAL);
				    }
                                 }
	                     }
<COMMENT> [^*()]+      { lparen = false; star = false; }
<YYINITIAL> "*)"             { return new Symbol(TokenConstants.ERROR, "Unmatched *)"); }

<YYINITIAL> "--".*           { ; }

<YYINITIAL> \"     { yybegin(STRING); }
<STRING> [^\"\\\n\0]+    {   
                             char[] spec = {'x'};
                             if (escquote == true) {
                                 if (yytext().charAt(0) == 't') spec[0] = '\t';
                                 if (yytext().charAt(0) == 'b') spec[0] = '\b';
                                 if (yytext().charAt(0) == 'f') spec[0] = '\f';
                                 if (yytext().charAt(0) == 'n') spec[0] = '\n';
                                 if (spec[0] != 'x') 
                                     stringtext = stringtext.append(new String(spec));
                                 escquote = false;
                             }
                             if (spec[0] == 'x') 
                                 stringtext = stringtext.append(yytext()); 
                             else
                                 if (yytext().length() > 1)
                                      stringtext = stringtext.append(yytext().substring(1)); 
                        }
<STRING> \"        {  
                       if (escnull == true) {
                            yybegin(YYINITIAL); 
                            escnull = false;
                            singlenull = false;
                            escquote = false;
                            stringtext = new StringBuilder("");
			    return new Symbol(TokenConstants.ERROR, "String contains escaped null character."); 
                       }
                       if (singlenull == true) {
                            yybegin(YYINITIAL); 
                            escnull = false;
                            escquote = false;
                            singlenull = false;
                            stringtext = new StringBuilder("");
			    return new Symbol(TokenConstants.ERROR, "String contains null character."); 
                       }
                       if (escquote == false) {
                           yybegin(YYINITIAL); 
                            int stl = stringtext.length();
                            if (stl >= MAX_STR_CONST) {
                                 stringtext = new StringBuilder("");
			         return new Symbol(TokenConstants.ERROR, "String constant too long"); 
                            }
                            else {
		                String myyytext = stringtext.toString();
                                stringtext = new StringBuilder("");
			        return new Symbol(TokenConstants.STR_CONST, new StringSymbol(myyytext, myyytext.length(), 0)); 
                            }
                       }
                       else {
                            stringtext = stringtext.append("\"");
                            escquote = false;
                       }
                   }
<STRING> [\0]        {  
                         if (escquote == true) {
                              escnull = true;
                              singlenull = false;
                              escquote = false;
                         }
                         else {
                              escnull = false;
                              singlenull = true;
                         }
                     }
<STRING> [\n]        {  
                         if (escquote == false) {
                              yybegin(YYINITIAL);
                              stringtext = new StringBuilder("");
			      return new Symbol(TokenConstants.ERROR, "Unterminated string constant"); 
                         }
                         else 
                             stringtext = stringtext.append("\n"); 
                             escquote = false;
                     }
<STRING> \\        { 
                       if (escquote == false) 
                            escquote = true; 
                       else {
                            stringtext = stringtext.append("\\"); 
                            escquote = false;
                       }
                  }

"<-"			{return new Symbol(TokenConstants.ASSIGN); }
[cC][lL][aA][sS][sS]			{return new Symbol(TokenConstants.CLASS); }
[eE][lL][sS][eE]			{return new Symbol(TokenConstants.ELSE); }
[fF][iI]			{return new Symbol(TokenConstants.FI); }
[iI][fF]			{return new Symbol(TokenConstants.IF); }
[iI][nN]			{return new Symbol(TokenConstants.IN); }
[iI][nN][hH][eE][rR][iI][tT][sS]		{return new Symbol(TokenConstants.INHERITS); }
[iI][sS][vV][o][iI][dD]		{return new Symbol(TokenConstants.ISVOID); }
[lL][eE][tT]			{return new Symbol(TokenConstants.LET); }
[lL][oO][oO][pP]			{return new Symbol(TokenConstants.LOOP); }
[pP][oO][oO][lL]			{return new Symbol(TokenConstants.POOL); }
[tT][hH][eE][nN]			{return new Symbol(TokenConstants.THEN); }
[w][hH][iI][lL][eE]			{return new Symbol(TokenConstants.WHILE); }
[cC][aA][sS][eE]			{return new Symbol(TokenConstants.CASE); }
[eE][sS][aA][cC]			{return new Symbol(TokenConstants.ESAC); }
[nN][eE][wW]			{return new Symbol(TokenConstants.NEW); }
[oO][fF]			{return new Symbol(TokenConstants.OF); }
[nN][oO][tT]			{return new Symbol(TokenConstants.NOT); }
[t][rR][uU][eE]			{return new Symbol(TokenConstants.BOOL_CONST, new Boolean(true)); }
[f][aA][lL][sS][eE]			{return new Symbol(TokenConstants.BOOL_CONST, new Boolean(false)); }

"="			{return new Symbol(TokenConstants.EQ); }
"<"			{return new Symbol(TokenConstants.LT); }
"<="			{return new Symbol(TokenConstants.LE); }

"("			{return new Symbol(TokenConstants.LPAREN); }
")"			{return new Symbol(TokenConstants.RPAREN); }
"{"			{return new Symbol(TokenConstants.LBRACE); }
"}"			{return new Symbol(TokenConstants.RBRACE); }
","			{return new Symbol(TokenConstants.COMMA); }
";"			{return new Symbol(TokenConstants.SEMI); }
":="			{return new Symbol(TokenConstants.ASSIGN); }
"+"			{return new Symbol(TokenConstants.PLUS); }
"-"			{return new Symbol(TokenConstants.MINUS); }
"*"			{return new Symbol(TokenConstants.MULT); }
"/"			{return new Symbol(TokenConstants.DIV); }
"."			{return new Symbol(TokenConstants.DOT); }
":"			{return new Symbol(TokenConstants.COLON); }
"~"			{return new Symbol(TokenConstants.NEG); }
"@"			{return new Symbol(TokenConstants.AT); }
"["			{return new Symbol(TokenConstants.ERROR, "["); }
"]"			{return new Symbol(TokenConstants.ERROR, "]"); }
">"			{return new Symbol(TokenConstants.ERROR, ">"); }
"'"			{return new Symbol(TokenConstants.ERROR, "'"); }
"!"			{return new Symbol(TokenConstants.ERROR, "!"); }

"#"                     {return new Symbol(TokenConstants.ERROR, "#"); }
"$"                     {return new Symbol(TokenConstants.ERROR, "$"); }
"%"                     {return new Symbol(TokenConstants.ERROR, "%"); }
"^"                     {return new Symbol(TokenConstants.ERROR, "^"); }
"&"                     {return new Symbol(TokenConstants.ERROR, "&"); }
"_"                     {return new Symbol(TokenConstants.ERROR, "_"); }
"?"                     {return new Symbol(TokenConstants.ERROR, "?"); }
"`"                     {return new Symbol(TokenConstants.ERROR, "`"); }
\\                      {return new Symbol(TokenConstants.ERROR, "\\"); }
"|"                     {return new Symbol(TokenConstants.ERROR, "|"); }
[\x01]                  {return new Symbol(TokenConstants.ERROR, "\\x01"); }
[\x02]                  {return new Symbol(TokenConstants.ERROR, "\\x02"); }
[\x03]                  {return new Symbol(TokenConstants.ERROR, "\\x03"); }
[\x04]                  {return new Symbol(TokenConstants.ERROR, "\\x04"); }



[0-9]+			{return new Symbol(TokenConstants.INT_CONST, new IntSymbol(yytext(), yytext().length(), 0)); } 
[A-Z][a-zA-Z_0-9]* 		{return new Symbol(TokenConstants.TYPEID, new IdSymbol(yytext(), yytext().length(), 0)); }
[a-z][a-zA-Z_0-9]* 		{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
[ \t\r\n\f\x0b]		{/* ignore white space */}

"=>"			{ /* Sample lexical rule for "=>" arrow.
                                     Further lexical rules should be defined
                                     here, after the last %% separator */
                                  return new Symbol(TokenConstants.DARROW); }

.                               { /* This rule should be the very last
                                     in your lexical specification and
                                     will match match everything not
                                     matched by other lexical rules. */
                                  System.err.println("LEXER BUG - UNMATCHED: " + yytext()); }
