/*
 *  The scanner definition for COOL.
 */
import java_cup.runtime.Symbol;


class CoolLexer implements java_cup.runtime.Scanner {
	private final int YY_BUFFER_SIZE = 512;
	private final int YY_F = -1;
	private final int YY_NO_STATE = -1;
	private final int YY_NOT_ACCEPT = 0;
	private final int YY_START = 1;
	private final int YY_END = 2;
	private final int YY_NO_ANCHOR = 4;
	private final int YY_BOL = 128;
	private final int YY_EOF = 129;

/*  Stuff enclosed in %{ %} is copied verbatim to the lexer class
 *  definition, all the extra variables/functions you want to use in the
 *  lexer actions should go here.  Don't remove or modify anything that
 *  was there initially.  */
    // Max size of string constants
    static int MAX_STR_CONST = 1025;
    // For assembling string constants
    StringBuffer string_buf = new StringBuffer();
    private int curr_lineno = 1;
    int get_curr_lineno() {
        curr_lineno = yyline+1;
	return curr_lineno;
    }
    private AbstractSymbol filename;
    void set_filename(String fname) {
	filename = AbstractTable.stringtable.addString(fname);
    }
    AbstractSymbol curr_filename() {
	return filename;
    }
    int commentDepth = 0;
    boolean star = false;
    boolean lparen = false;
    boolean escquote = false;
    boolean escnull = false;
    boolean singlenull = false;
    StringBuilder stringtext = new StringBuilder("");
	private java.io.BufferedReader yy_reader;
	private int yy_buffer_index;
	private int yy_buffer_read;
	private int yy_buffer_start;
	private int yy_buffer_end;
	private char yy_buffer[];
	private int yyline;
	private boolean yy_at_bol;
	private int yy_lexical_state;

	CoolLexer (java.io.Reader reader) {
		this ();
		if (null == reader) {
			throw (new Error("Error: Bad input stream initializer."));
		}
		yy_reader = new java.io.BufferedReader(reader);
	}

	CoolLexer (java.io.InputStream instream) {
		this ();
		if (null == instream) {
			throw (new Error("Error: Bad input stream initializer."));
		}
		yy_reader = new java.io.BufferedReader(new java.io.InputStreamReader(instream));
	}

	private CoolLexer () {
		yy_buffer = new char[YY_BUFFER_SIZE];
		yy_buffer_read = 0;
		yy_buffer_index = 0;
		yy_buffer_start = 0;
		yy_buffer_end = 0;
		yyline = 0;
		yy_at_bol = true;
		yy_lexical_state = YYINITIAL;

/*  Stuff enclosed in %init{ %init} is copied verbatim to the lexer
 *  class constructor, all the extra initialization you want to do should
 *  go here.  Don't remove or modify anything that was there initially. */
    // empty for now
	}

	private boolean yy_eof_done = false;
	private final int STRING = 2;
	private final int YYINITIAL = 0;
	private final int COMMENT = 1;
	private final int yy_state_dtrans[] = {
		0,
		78,
		97
	};
	private void yybegin (int state) {
		yy_lexical_state = state;
	}
	private int yy_advance ()
		throws java.io.IOException {
		int next_read;
		int i;
		int j;

		if (yy_buffer_index < yy_buffer_read) {
			return yy_buffer[yy_buffer_index++];
		}

		if (0 != yy_buffer_start) {
			i = yy_buffer_start;
			j = 0;
			while (i < yy_buffer_read) {
				yy_buffer[j] = yy_buffer[i];
				++i;
				++j;
			}
			yy_buffer_end = yy_buffer_end - yy_buffer_start;
			yy_buffer_start = 0;
			yy_buffer_read = j;
			yy_buffer_index = j;
			next_read = yy_reader.read(yy_buffer,
					yy_buffer_read,
					yy_buffer.length - yy_buffer_read);
			if (-1 == next_read) {
				return YY_EOF;
			}
			yy_buffer_read = yy_buffer_read + next_read;
		}

		while (yy_buffer_index >= yy_buffer_read) {
			if (yy_buffer_index >= yy_buffer.length) {
				yy_buffer = yy_double(yy_buffer);
			}
			next_read = yy_reader.read(yy_buffer,
					yy_buffer_read,
					yy_buffer.length - yy_buffer_read);
			if (-1 == next_read) {
				return YY_EOF;
			}
			yy_buffer_read = yy_buffer_read + next_read;
		}
		return yy_buffer[yy_buffer_index++];
	}
	private void yy_move_end () {
		if (yy_buffer_end > yy_buffer_start &&
		    '\n' == yy_buffer[yy_buffer_end-1])
			yy_buffer_end--;
		if (yy_buffer_end > yy_buffer_start &&
		    '\r' == yy_buffer[yy_buffer_end-1])
			yy_buffer_end--;
	}
	private boolean yy_last_was_cr=false;
	private void yy_mark_start () {
		int i;
		for (i = yy_buffer_start; i < yy_buffer_index; ++i) {
			if ('\n' == yy_buffer[i] && !yy_last_was_cr) {
				++yyline;
			}
			if ('\r' == yy_buffer[i]) {
				++yyline;
				yy_last_was_cr=true;
			} else yy_last_was_cr=false;
		}
		yy_buffer_start = yy_buffer_index;
	}
	private void yy_mark_end () {
		yy_buffer_end = yy_buffer_index;
	}
	private void yy_to_mark () {
		yy_buffer_index = yy_buffer_end;
		yy_at_bol = (yy_buffer_end > yy_buffer_start) &&
		            ('\r' == yy_buffer[yy_buffer_end-1] ||
		             '\n' == yy_buffer[yy_buffer_end-1] ||
		             2028/*LS*/ == yy_buffer[yy_buffer_end-1] ||
		             2029/*PS*/ == yy_buffer[yy_buffer_end-1]);
	}
	private java.lang.String yytext () {
		return (new java.lang.String(yy_buffer,
			yy_buffer_start,
			yy_buffer_end - yy_buffer_start));
	}
	private int yylength () {
		return yy_buffer_end - yy_buffer_start;
	}
	private char[] yy_double (char buf[]) {
		int i;
		char newbuf[];
		newbuf = new char[2*buf.length];
		for (i = 0; i < buf.length; ++i) {
			newbuf[i] = buf[i];
		}
		return newbuf;
	}
	private final int YY_E_INTERNAL = 0;
	private final int YY_E_MATCH = 1;
	private java.lang.String yy_error_string[] = {
		"Error: Internal error.\n",
		"Error: Unmatched input.\n"
	};
	private void yy_error (int code,boolean fatal) {
		java.lang.System.out.print(yy_error_string[code]);
		java.lang.System.out.flush();
		if (fatal) {
			throw new Error("Fatal Error.\n");
		}
	}
	private int[][] unpackFromString(int size1, int size2, String st) {
		int colonIndex = -1;
		String lengthString;
		int sequenceLength = 0;
		int sequenceInteger = 0;

		int commaIndex;
		String workString;

		int res[][] = new int[size1][size2];
		for (int i= 0; i < size1; i++) {
			for (int j= 0; j < size2; j++) {
				if (sequenceLength != 0) {
					res[i][j] = sequenceInteger;
					sequenceLength--;
					continue;
				}
				commaIndex = st.indexOf(',');
				workString = (commaIndex==-1) ? st :
					st.substring(0, commaIndex);
				st = st.substring(commaIndex+1);
				colonIndex = workString.indexOf(':');
				if (colonIndex == -1) {
					res[i][j]=Integer.parseInt(workString);
					continue;
				}
				lengthString =
					workString.substring(colonIndex+1);
				sequenceLength=Integer.parseInt(lengthString);
				workString=workString.substring(0,colonIndex);
				sequenceInteger=Integer.parseInt(workString);
				res[i][j] = sequenceInteger;
				sequenceLength--;
			}
		}
		return res;
	}
	private int yy_acpt[] = {
		/* 0 */ YY_NOT_ACCEPT,
		/* 1 */ YY_NO_ANCHOR,
		/* 2 */ YY_NO_ANCHOR,
		/* 3 */ YY_NO_ANCHOR,
		/* 4 */ YY_NO_ANCHOR,
		/* 5 */ YY_NO_ANCHOR,
		/* 6 */ YY_NO_ANCHOR,
		/* 7 */ YY_NO_ANCHOR,
		/* 8 */ YY_NO_ANCHOR,
		/* 9 */ YY_NO_ANCHOR,
		/* 10 */ YY_NO_ANCHOR,
		/* 11 */ YY_NO_ANCHOR,
		/* 12 */ YY_NO_ANCHOR,
		/* 13 */ YY_NO_ANCHOR,
		/* 14 */ YY_NO_ANCHOR,
		/* 15 */ YY_NO_ANCHOR,
		/* 16 */ YY_NO_ANCHOR,
		/* 17 */ YY_NO_ANCHOR,
		/* 18 */ YY_NO_ANCHOR,
		/* 19 */ YY_NO_ANCHOR,
		/* 20 */ YY_NO_ANCHOR,
		/* 21 */ YY_NO_ANCHOR,
		/* 22 */ YY_NO_ANCHOR,
		/* 23 */ YY_NO_ANCHOR,
		/* 24 */ YY_NO_ANCHOR,
		/* 25 */ YY_NO_ANCHOR,
		/* 26 */ YY_NO_ANCHOR,
		/* 27 */ YY_NO_ANCHOR,
		/* 28 */ YY_NO_ANCHOR,
		/* 29 */ YY_NO_ANCHOR,
		/* 30 */ YY_NO_ANCHOR,
		/* 31 */ YY_NO_ANCHOR,
		/* 32 */ YY_NO_ANCHOR,
		/* 33 */ YY_NO_ANCHOR,
		/* 34 */ YY_NO_ANCHOR,
		/* 35 */ YY_NO_ANCHOR,
		/* 36 */ YY_NO_ANCHOR,
		/* 37 */ YY_NO_ANCHOR,
		/* 38 */ YY_NO_ANCHOR,
		/* 39 */ YY_NO_ANCHOR,
		/* 40 */ YY_NO_ANCHOR,
		/* 41 */ YY_NO_ANCHOR,
		/* 42 */ YY_NO_ANCHOR,
		/* 43 */ YY_NO_ANCHOR,
		/* 44 */ YY_NO_ANCHOR,
		/* 45 */ YY_NO_ANCHOR,
		/* 46 */ YY_NO_ANCHOR,
		/* 47 */ YY_NO_ANCHOR,
		/* 48 */ YY_NO_ANCHOR,
		/* 49 */ YY_NO_ANCHOR,
		/* 50 */ YY_NO_ANCHOR,
		/* 51 */ YY_NO_ANCHOR,
		/* 52 */ YY_NO_ANCHOR,
		/* 53 */ YY_NO_ANCHOR,
		/* 54 */ YY_NO_ANCHOR,
		/* 55 */ YY_NO_ANCHOR,
		/* 56 */ YY_NO_ANCHOR,
		/* 57 */ YY_NO_ANCHOR,
		/* 58 */ YY_NO_ANCHOR,
		/* 59 */ YY_NO_ANCHOR,
		/* 60 */ YY_NO_ANCHOR,
		/* 61 */ YY_NO_ANCHOR,
		/* 62 */ YY_NO_ANCHOR,
		/* 63 */ YY_NO_ANCHOR,
		/* 64 */ YY_NO_ANCHOR,
		/* 65 */ YY_NO_ANCHOR,
		/* 66 */ YY_NO_ANCHOR,
		/* 67 */ YY_NO_ANCHOR,
		/* 68 */ YY_NO_ANCHOR,
		/* 69 */ YY_NO_ANCHOR,
		/* 70 */ YY_NO_ANCHOR,
		/* 71 */ YY_NO_ANCHOR,
		/* 72 */ YY_NO_ANCHOR,
		/* 73 */ YY_NO_ANCHOR,
		/* 74 */ YY_NO_ANCHOR,
		/* 75 */ YY_NO_ANCHOR,
		/* 76 */ YY_NO_ANCHOR,
		/* 77 */ YY_NO_ANCHOR,
		/* 78 */ YY_NOT_ACCEPT,
		/* 79 */ YY_NO_ANCHOR,
		/* 80 */ YY_NO_ANCHOR,
		/* 81 */ YY_NO_ANCHOR,
		/* 82 */ YY_NO_ANCHOR,
		/* 83 */ YY_NO_ANCHOR,
		/* 84 */ YY_NO_ANCHOR,
		/* 85 */ YY_NO_ANCHOR,
		/* 86 */ YY_NO_ANCHOR,
		/* 87 */ YY_NO_ANCHOR,
		/* 88 */ YY_NO_ANCHOR,
		/* 89 */ YY_NO_ANCHOR,
		/* 90 */ YY_NO_ANCHOR,
		/* 91 */ YY_NO_ANCHOR,
		/* 92 */ YY_NO_ANCHOR,
		/* 93 */ YY_NO_ANCHOR,
		/* 94 */ YY_NO_ANCHOR,
		/* 95 */ YY_NO_ANCHOR,
		/* 96 */ YY_NO_ANCHOR,
		/* 97 */ YY_NOT_ACCEPT,
		/* 98 */ YY_NO_ANCHOR,
		/* 99 */ YY_NO_ANCHOR,
		/* 100 */ YY_NO_ANCHOR,
		/* 101 */ YY_NO_ANCHOR,
		/* 102 */ YY_NO_ANCHOR,
		/* 103 */ YY_NO_ANCHOR,
		/* 104 */ YY_NO_ANCHOR,
		/* 105 */ YY_NO_ANCHOR,
		/* 106 */ YY_NO_ANCHOR,
		/* 107 */ YY_NO_ANCHOR,
		/* 108 */ YY_NO_ANCHOR,
		/* 109 */ YY_NO_ANCHOR,
		/* 110 */ YY_NO_ANCHOR,
		/* 111 */ YY_NO_ANCHOR,
		/* 112 */ YY_NO_ANCHOR,
		/* 113 */ YY_NO_ANCHOR,
		/* 114 */ YY_NO_ANCHOR,
		/* 115 */ YY_NO_ANCHOR,
		/* 116 */ YY_NO_ANCHOR,
		/* 117 */ YY_NO_ANCHOR,
		/* 118 */ YY_NO_ANCHOR,
		/* 119 */ YY_NO_ANCHOR,
		/* 120 */ YY_NO_ANCHOR,
		/* 121 */ YY_NO_ANCHOR,
		/* 122 */ YY_NO_ANCHOR,
		/* 123 */ YY_NO_ANCHOR,
		/* 124 */ YY_NO_ANCHOR,
		/* 125 */ YY_NO_ANCHOR,
		/* 126 */ YY_NO_ANCHOR,
		/* 127 */ YY_NO_ANCHOR,
		/* 128 */ YY_NO_ANCHOR,
		/* 129 */ YY_NO_ANCHOR,
		/* 130 */ YY_NO_ANCHOR,
		/* 131 */ YY_NO_ANCHOR,
		/* 132 */ YY_NO_ANCHOR,
		/* 133 */ YY_NO_ANCHOR,
		/* 134 */ YY_NO_ANCHOR,
		/* 135 */ YY_NO_ANCHOR,
		/* 136 */ YY_NO_ANCHOR,
		/* 137 */ YY_NO_ANCHOR,
		/* 138 */ YY_NO_ANCHOR,
		/* 139 */ YY_NO_ANCHOR,
		/* 140 */ YY_NO_ANCHOR,
		/* 141 */ YY_NO_ANCHOR,
		/* 142 */ YY_NO_ANCHOR,
		/* 143 */ YY_NO_ANCHOR,
		/* 144 */ YY_NO_ANCHOR,
		/* 145 */ YY_NO_ANCHOR,
		/* 146 */ YY_NO_ANCHOR,
		/* 147 */ YY_NO_ANCHOR,
		/* 148 */ YY_NO_ANCHOR,
		/* 149 */ YY_NO_ANCHOR,
		/* 150 */ YY_NO_ANCHOR,
		/* 151 */ YY_NO_ANCHOR,
		/* 152 */ YY_NO_ANCHOR,
		/* 153 */ YY_NO_ANCHOR,
		/* 154 */ YY_NO_ANCHOR,
		/* 155 */ YY_NO_ANCHOR,
		/* 156 */ YY_NO_ANCHOR,
		/* 157 */ YY_NO_ANCHOR,
		/* 158 */ YY_NO_ANCHOR,
		/* 159 */ YY_NO_ANCHOR,
		/* 160 */ YY_NO_ANCHOR,
		/* 161 */ YY_NO_ANCHOR,
		/* 162 */ YY_NO_ANCHOR,
		/* 163 */ YY_NO_ANCHOR,
		/* 164 */ YY_NO_ANCHOR,
		/* 165 */ YY_NO_ANCHOR,
		/* 166 */ YY_NO_ANCHOR,
		/* 167 */ YY_NO_ANCHOR,
		/* 168 */ YY_NO_ANCHOR,
		/* 169 */ YY_NO_ANCHOR,
		/* 170 */ YY_NO_ANCHOR,
		/* 171 */ YY_NO_ANCHOR,
		/* 172 */ YY_NO_ANCHOR,
		/* 173 */ YY_NO_ANCHOR,
		/* 174 */ YY_NO_ANCHOR,
		/* 175 */ YY_NO_ANCHOR,
		/* 176 */ YY_NO_ANCHOR
	};
	private int yy_cmap[] = unpackFromString(1,130,
"10,58,59,60,61,8:4,78,4,78:2,9,8:18,78,48,7,49,50,51,53,47,1,3,2,39,36,5,41" +
",40,62:10,38,37,11,33,46,55,43,63,64,65,66,67,17,64,68,69,64:2,70,64,71,26," +
"72,64,73,74,22,75,76,29,64:3,44,6,45,52,54,56,14,77,12,25,16,32,77,20,18,77" +
":2,13,77,19,24,27,77,21,15,30,31,23,28,77:3,34,57,35,42,8,0:2")[0];

	private int yy_rmap[] = unpackFromString(1,177,
"0,1,2,3,1:2,4,1:3,5,6,7,8,1:4,9,1:23,10,1:2,11,1:2,12,13,14,13,1:2,13:7,12," +
"13:7,1:3,15,16,1:4,17,18,19,13,12,20,12:8,13,12:4,21,22,23,24,25,26,27,28,2" +
"9,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,5" +
"4,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,7" +
"9,80,81,82,83,84,85,86,87,88,89,90,13,12,91,92,93,94,95,96,97,98")[0];

	private int yy_nxt[][] = unpackFromString(99,79,
"1,2,3,4,5,6,7,8,9,5,9,10,11,129,167:2,169,12,79,131,167:2,80,167,98,167,99," +
"171,173,168,175,167,100,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29," +
"30,31,32,33,34,35,36,37,38,39,40,41,42,168:2,170,168,172,168,101,130,132,17" +
"4,168:4,167,5,-1:81,43,-1:79,44,-1:80,45,-1:78,46,-1:27,47,-1:57,167,176,13" +
"3,167:18,-1:21,167,-1:7,167,133,167:6,176,167:7,-1:13,168:6,48,168:14,-1:21" +
",168,-1:7,168:7,48,168:8,-1:47,52,-1:65,53,-1:107,42,-1:17,45:3,-1,45:4,-1," +
"45:69,-1:12,168:21,-1:21,168,-1:7,168:16,-1:13,167:21,-1:21,167,-1:7,167:16" +
",-1:13,167:8,157,167:12,-1:21,167,-1:7,167:6,157,167:9,-1:5,72:75,-1,73:3,-" +
"1,73,-1:2,73:2,-1,73:68,1,69,70,71,72:75,-1:12,167:3,141,167,49,167,50,167:" +
"12,49,-1:21,167,-1:7,167:9,50,167:2,141,167:3,-1:13,168:8,134,168:12,-1:21," +
"168,-1:7,168:6,134,168:9,-1:13,168:8,154,168:12,-1:21,168,-1:7,168:6,154,16" +
"8:9,-1,1,73:3,74,73,75,76,73:2,77,73:68,-1:12,167:5,51,167:14,51,-1:21,167," +
"-1:7,167:16,-1:13,168:5,84,168:14,84,-1:21,168,-1:7,168:16,-1:13,167:2,151," +
"167:3,81,167:14,-1:21,167,-1:7,167,151,167:5,81,167:8,-1:13,168:3,144,168,8" +
"2,168,83,168:12,82,-1:21,168,-1:7,168:9,83,168:2,144,168:3,-1:13,167:10,54," +
"167:7,54,167:2,-1:21,167,-1:7,167:16,-1:13,168:10,85,168:7,85,168:2,-1:21,1" +
"68,-1:7,168:16,-1:13,167:16,55:2,167:3,-1:21,167,-1:7,167:16,-1:13,168:16,8" +
"6:2,168:3,-1:21,168,-1:7,168:16,-1:13,167:10,56,167:7,56,167:2,-1:21,167,-1" +
":7,167:16,-1:13,168:10,87,168:7,87,168:2,-1:21,168,-1:7,168:16,-1:13,167:4," +
"57,167:16,-1:21,167,-1:7,167:5,57,167:10,-1:13,168:7,61,168:13,-1:21,168,-1" +
":7,168:9,61,168:6,-1:13,167:15,58,167:5,-1:21,167,-1:7,167:10,58,167:5,-1:1" +
"3,168:4,88,168:16,-1:21,168,-1:7,168:5,88,168:10,-1:13,167:4,59,167:16,-1:2" +
"1,167,-1:7,167:5,59,167:10,-1:13,168:4,90,168:16,-1:21,168,-1:7,168:5,90,16" +
"8:10,-1:13,60,167:20,-1:21,167,-1:7,167:3,60,167:12,-1:13,91,168:20,-1:21,1" +
"68,-1:7,168:3,91,168:12,-1:13,167,62,167:19,-1:21,167,-1:7,167:8,62,167:7,-" +
"1:13,168:15,89,168:5,-1:21,168,-1:7,168:10,89,168:5,-1:13,167:7,92,167:13,-" +
"1:21,167,-1:7,167:9,92,167:6,-1:13,168,93,168:19,-1:21,168,-1:7,168:8,93,16" +
"8:7,-1:13,167:4,63,167:16,-1:21,167,-1:7,167:5,63,167:10,-1:13,168:3,94,168" +
":17,-1:21,168,-1:7,168:12,94,168:3,-1:13,167:3,64,167:17,-1:21,167,-1:7,167" +
":12,64,167:3,-1:13,168:13,95,168:7,-1:21,168,-1:7,168:4,95,168:11,-1:13,167" +
":4,65,167:16,-1:21,167,-1:7,167:5,65,167:10,-1:13,168:3,96,168:17,-1:21,168" +
",-1:7,168:12,96,168:3,-1:13,167:4,66,167:16,-1:21,167,-1:7,167:5,66,167:10," +
"-1:13,167:13,67,167:7,-1:21,167,-1:7,167:4,67,167:11,-1:13,167:3,68,167:17," +
"-1:21,167,-1:7,167:12,68,167:3,-1:13,167:4,102,167:7,135,167,135,167:6,-1:2" +
"1,167,-1:7,167:5,102,167:10,-1:13,168:4,103,168:7,146,168,146,168:6,-1:21,1" +
"68,-1:7,168:5,103,168:10,-1:13,167:4,104,167:7,106,167,106,167:6,-1:21,167," +
"-1:7,167:5,104,167:10,-1:13,168:4,105,168:7,107,168,107,168:6,-1:21,168,-1:" +
"7,168:5,105,168:10,-1:13,167:3,108,167:17,-1:21,167,-1:7,167:12,108,167:3,-" +
"1:13,168:4,109,168:16,-1:21,168,-1:7,168:5,109,168:10,-1:13,167:12,110,167," +
"110,167:6,-1:21,167,-1:7,167:16,-1:13,168:2,150,168:18,-1:21,168,-1:7,168,1" +
"50,168:14,-1:13,167:3,112,167:17,-1:21,167,-1:7,167:12,112,167:3,-1:13,168:" +
"3,111,168:17,-1:21,168,-1:7,168:12,111,168:3,-1:13,167:2,114,167:18,-1:21,1" +
"67,-1:7,167,114,167:14,-1:13,168:3,113,168:17,-1:21,168,-1:7,168:12,113,168" +
":3,-1:13,167:11,155,167:9,-1:21,167,-1:7,167:14,155,167,-1:13,168:2,115,168" +
":18,-1:21,168,-1:7,168,115,168:14,-1:13,167:12,116,167,116,167:6,-1:21,167," +
"-1:7,167:16,-1:13,168:11,152,168:9,-1:21,168,-1:7,168:14,152,168,-1:13,167:" +
"6,159,167:14,-1:21,167,-1:7,167:7,159,167:8,-1:13,168:12,117,168,117,168:6," +
"-1:21,168,-1:7,168:16,-1:13,167:4,118,167:16,-1:21,167,-1:7,167:5,118,167:1" +
"0,-1:13,168:12,119,168,119,168:6,-1:21,168,-1:7,168:16,-1:13,167:19,120,167" +
",-1:21,167,-1:7,167:13,120,167:2,-1:13,168:3,121,168:17,-1:21,168,-1:7,168:" +
"12,121,168:3,-1:13,167,161,167:19,-1:21,167,-1:7,167:8,161,167:7,-1:13,168:" +
"12,156,168:8,-1:21,168,-1:7,168:16,-1:13,167:3,122,167:17,-1:21,167,-1:7,16" +
"7:12,122,167:3,-1:13,168:4,158,168:16,-1:21,168,-1:7,168:5,158,168:10,-1:13" +
",167:12,163,167:8,-1:21,167,-1:7,167:16,-1:13,168:6,123,168:14,-1:21,168,-1" +
":7,168:7,123,168:8,-1:13,167:4,164,167:16,-1:21,167,-1:7,167:5,164,167:10,-" +
"1:13,168:9,160,168:11,-1:21,168,-1:7,168:11,160,168:4,-1:13,167,124,167:19," +
"-1:21,167,-1:7,167:8,124,167:7,-1:13,168:6,162,168:14,-1:21,168,-1:7,168:7," +
"162,168:8,-1:13,167:3,126,167:17,-1:21,167,-1:7,167:12,126,167:3,-1:13,168:" +
"10,125,168:7,125,168:2,-1:21,168,-1:7,168:16,-1:13,167:6,127,167:14,-1:21,1" +
"67,-1:7,167:7,127,167:8,-1:13,167:9,165,167:11,-1:21,167,-1:7,167:11,165,16" +
"7:4,-1:13,167:6,166,167:14,-1:21,167,-1:7,167:7,166,167:8,-1:13,167:10,128," +
"167:7,128,167:2,-1:21,167,-1:7,167:16,-1:13,167,137,167,139,167:17,-1:21,16" +
"7,-1:7,167:8,137,167:3,139,167:3,-1:13,168,136,138,168:18,-1:21,168,-1:7,16" +
"8,138,168:6,136,168:7,-1:13,167:12,143,167,143,167:6,-1:21,167,-1:7,167:16," +
"-1:13,168,140,168,142,168:17,-1:21,168,-1:7,168:8,140,168:3,142,168:3,-1:13" +
",167:8,145,167:12,-1:21,167,-1:7,167:6,145,167:9,-1:13,168:12,148,168,148,1" +
"68:6,-1:21,168,-1:7,168:16,-1:13,167:8,147,149,167:11,-1:21,167,-1:7,167:6," +
"147,167:4,149,167:4,-1:13,167:2,153,167:18,-1:21,167,-1:7,167,153,167:14,-1");

	public java_cup.runtime.Symbol next_token ()
		throws java.io.IOException {
		int yy_lookahead;
		int yy_anchor = YY_NO_ANCHOR;
		int yy_state = yy_state_dtrans[yy_lexical_state];
		int yy_next_state = YY_NO_STATE;
		int yy_last_accept_state = YY_NO_STATE;
		boolean yy_initial = true;
		int yy_this_accept;

		yy_mark_start();
		yy_this_accept = yy_acpt[yy_state];
		if (YY_NOT_ACCEPT != yy_this_accept) {
			yy_last_accept_state = yy_state;
			yy_mark_end();
		}
		while (true) {
			if (yy_initial && yy_at_bol) yy_lookahead = YY_BOL;
			else yy_lookahead = yy_advance();
			yy_next_state = YY_F;
			yy_next_state = yy_nxt[yy_rmap[yy_state]][yy_cmap[yy_lookahead]];
			if (YY_EOF == yy_lookahead && true == yy_initial) {

/*  Stuff enclosed in %eofval{ %eofval} specifies java code that is
 *  executed when end-of-file is reached.  If you use multiple lexical
 *  states and want to do something special if an EOF is encountered in
 *  one of those states, place your code in the switch statement.
 *  Ultimately, you should return the EOF symbol, or your lexer won't
 *  work.  */
    switch(yy_lexical_state) {
    case YYINITIAL:
	/* nothing special to do in the initial state */
	break;
    case COMMENT:
        yybegin(YYINITIAL);
        return new Symbol(TokenConstants.ERROR, "EOF in comment");
    case STRING:
        yybegin(YYINITIAL);
        escquote = false;
        stringtext = new StringBuilder("");
        return new Symbol(TokenConstants.ERROR, "EOF in string constant");
    }
    return new Symbol(TokenConstants.EOF);
			}
			if (YY_F != yy_next_state) {
				yy_state = yy_next_state;
				yy_initial = false;
				yy_this_accept = yy_acpt[yy_state];
				if (YY_NOT_ACCEPT != yy_this_accept) {
					yy_last_accept_state = yy_state;
					yy_mark_end();
				}
			}
			else {
				if (YY_NO_STATE == yy_last_accept_state) {
					throw (new Error("Lexical Error: Unmatched Input."));
				}
				else {
					yy_anchor = yy_acpt[yy_last_accept_state];
					if (0 != (YY_END & yy_anchor)) {
						yy_move_end();
					}
					yy_to_mark();
					switch (yy_last_accept_state) {
					case 1:
						
					case -2:
						break;
					case 2:
						{return new Symbol(TokenConstants.LPAREN); }
					case -3:
						break;
					case 3:
						{return new Symbol(TokenConstants.MULT); }
					case -4:
						break;
					case 4:
						{return new Symbol(TokenConstants.RPAREN); }
					case -5:
						break;
					case 5:
						{/* ignore white space */}
					case -6:
						break;
					case 6:
						{return new Symbol(TokenConstants.MINUS); }
					case -7:
						break;
					case 7:
						{return new Symbol(TokenConstants.ERROR, "\\"); }
					case -8:
						break;
					case 8:
						{ yybegin(STRING); }
					case -9:
						break;
					case 9:
						{ /* This rule should be the very last
                                     in your lexical specification and
                                     will match match everything not
                                     matched by other lexical rules. */
                                  System.err.println("LEXER BUG - UNMATCHED: " + yytext()); }
					case -10:
						break;
					case 10:
						{return new Symbol(TokenConstants.LT); }
					case -11:
						break;
					case 11:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -12:
						break;
					case 12:
						{return new Symbol(TokenConstants.TYPEID, new IdSymbol(yytext(), yytext().length(), 0)); }
					case -13:
						break;
					case 13:
						{return new Symbol(TokenConstants.EQ); }
					case -14:
						break;
					case 14:
						{return new Symbol(TokenConstants.LBRACE); }
					case -15:
						break;
					case 15:
						{return new Symbol(TokenConstants.RBRACE); }
					case -16:
						break;
					case 16:
						{return new Symbol(TokenConstants.COMMA); }
					case -17:
						break;
					case 17:
						{return new Symbol(TokenConstants.SEMI); }
					case -18:
						break;
					case 18:
						{return new Symbol(TokenConstants.COLON); }
					case -19:
						break;
					case 19:
						{return new Symbol(TokenConstants.PLUS); }
					case -20:
						break;
					case 20:
						{return new Symbol(TokenConstants.DIV); }
					case -21:
						break;
					case 21:
						{return new Symbol(TokenConstants.DOT); }
					case -22:
						break;
					case 22:
						{return new Symbol(TokenConstants.NEG); }
					case -23:
						break;
					case 23:
						{return new Symbol(TokenConstants.AT); }
					case -24:
						break;
					case 24:
						{return new Symbol(TokenConstants.ERROR, "["); }
					case -25:
						break;
					case 25:
						{return new Symbol(TokenConstants.ERROR, "]"); }
					case -26:
						break;
					case 26:
						{return new Symbol(TokenConstants.ERROR, ">"); }
					case -27:
						break;
					case 27:
						{return new Symbol(TokenConstants.ERROR, "'"); }
					case -28:
						break;
					case 28:
						{return new Symbol(TokenConstants.ERROR, "!"); }
					case -29:
						break;
					case 29:
						{return new Symbol(TokenConstants.ERROR, "#"); }
					case -30:
						break;
					case 30:
						{return new Symbol(TokenConstants.ERROR, "$"); }
					case -31:
						break;
					case 31:
						{return new Symbol(TokenConstants.ERROR, "%"); }
					case -32:
						break;
					case 32:
						{return new Symbol(TokenConstants.ERROR, "^"); }
					case -33:
						break;
					case 33:
						{return new Symbol(TokenConstants.ERROR, "&"); }
					case -34:
						break;
					case 34:
						{return new Symbol(TokenConstants.ERROR, "_"); }
					case -35:
						break;
					case 35:
						{return new Symbol(TokenConstants.ERROR, "?"); }
					case -36:
						break;
					case 36:
						{return new Symbol(TokenConstants.ERROR, "`"); }
					case -37:
						break;
					case 37:
						{return new Symbol(TokenConstants.ERROR, "|"); }
					case -38:
						break;
					case 38:
						{return new Symbol(TokenConstants.ERROR, "\\x01"); }
					case -39:
						break;
					case 39:
						{return new Symbol(TokenConstants.ERROR, "\\x02"); }
					case -40:
						break;
					case 40:
						{return new Symbol(TokenConstants.ERROR, "\\x03"); }
					case -41:
						break;
					case 41:
						{return new Symbol(TokenConstants.ERROR, "\\x04"); }
					case -42:
						break;
					case 42:
						{return new Symbol(TokenConstants.INT_CONST, new IntSymbol(yytext(), yytext().length(), 0)); }
					case -43:
						break;
					case 43:
						{ yybegin(COMMENT); commentDepth++; }
					case -44:
						break;
					case 44:
						{ return new Symbol(TokenConstants.ERROR, "Unmatched *)"); }
					case -45:
						break;
					case 45:
						{ ; }
					case -46:
						break;
					case 46:
						{return new Symbol(TokenConstants.ASSIGN); }
					case -47:
						break;
					case 47:
						{return new Symbol(TokenConstants.LE); }
					case -48:
						break;
					case 48:
						{return new Symbol(TokenConstants.FI); }
					case -49:
						break;
					case 49:
						{return new Symbol(TokenConstants.IF); }
					case -50:
						break;
					case 50:
						{return new Symbol(TokenConstants.IN); }
					case -51:
						break;
					case 51:
						{return new Symbol(TokenConstants.OF); }
					case -52:
						break;
					case 52:
						{ /* Sample lexical rule for "=>" arrow.
                                     Further lexical rules should be defined
                                     here, after the last %% separator */
                                  return new Symbol(TokenConstants.DARROW); }
					case -53:
						break;
					case 53:
						{return new Symbol(TokenConstants.ASSIGN); }
					case -54:
						break;
					case 54:
						{return new Symbol(TokenConstants.LET); }
					case -55:
						break;
					case 55:
						{return new Symbol(TokenConstants.NEW); }
					case -56:
						break;
					case 56:
						{return new Symbol(TokenConstants.NOT); }
					case -57:
						break;
					case 57:
						{return new Symbol(TokenConstants.CASE); }
					case -58:
						break;
					case 58:
						{return new Symbol(TokenConstants.LOOP); }
					case -59:
						break;
					case 59:
						{return new Symbol(TokenConstants.ELSE); }
					case -60:
						break;
					case 60:
						{return new Symbol(TokenConstants.ESAC); }
					case -61:
						break;
					case 61:
						{return new Symbol(TokenConstants.THEN); }
					case -62:
						break;
					case 62:
						{return new Symbol(TokenConstants.POOL); }
					case -63:
						break;
					case 63:
						{return new Symbol(TokenConstants.BOOL_CONST, new Boolean(true)); }
					case -64:
						break;
					case 64:
						{return new Symbol(TokenConstants.CLASS); }
					case -65:
						break;
					case 65:
						{return new Symbol(TokenConstants.WHILE); }
					case -66:
						break;
					case 66:
						{return new Symbol(TokenConstants.BOOL_CONST, new Boolean(false)); }
					case -67:
						break;
					case 67:
						{return new Symbol(TokenConstants.ISVOID); }
					case -68:
						break;
					case 68:
						{return new Symbol(TokenConstants.INHERITS); }
					case -69:
						break;
					case 69:
						{ if (lparen == false) 
                              lparen = true;  
                          else
                              lparen = false;
                        }
					case -70:
						break;
					case 70:
						{ 
                            if (lparen == true) {
                                commentDepth++;
                                lparen = false;
                            }
                            else
                                star = true;  
                        }
					case -71:
						break;
					case 71:
						{     if (star == true) {
                                    star = false;
				    commentDepth--; 
				    if (commentDepth == 0) {
				  	yybegin(YYINITIAL);
				    }
                                 }
	                     }
					case -72:
						break;
					case 72:
						{ lparen = false; star = false; }
					case -73:
						break;
					case 73:
						{   
                             char[] spec = {'x'};
                             if (escquote == true) {
                                 if (yytext().charAt(0) == 't') spec[0] = '\t';
                                 if (yytext().charAt(0) == 'b') spec[0] = '\b';
                                 if (yytext().charAt(0) == 'f') spec[0] = '\f';
                                 if (yytext().charAt(0) == 'n') spec[0] = '\n';
                                 if (spec[0] != 'x') 
                                     stringtext = stringtext.append(new String(spec));
                                 escquote = false;
                             }
                             if (spec[0] == 'x') 
                                 stringtext = stringtext.append(yytext()); 
                             else
                                 if (yytext().length() > 1)
                                      stringtext = stringtext.append(yytext().substring(1)); 
                        }
					case -74:
						break;
					case 74:
						{  
                         if (escquote == false) {
                              yybegin(YYINITIAL);
                              stringtext = new StringBuilder("");
			      return new Symbol(TokenConstants.ERROR, "Unterminated string constant"); 
                         }
                         else 
                             stringtext = stringtext.append("\n"); 
                             escquote = false;
                     }
					case -75:
						break;
					case 75:
						{ 
                       if (escquote == false) 
                            escquote = true; 
                       else {
                            stringtext = stringtext.append("\\"); 
                            escquote = false;
                       }
                  }
					case -76:
						break;
					case 76:
						{  
                       if (escnull == true) {
                            yybegin(YYINITIAL); 
                            escnull = false;
                            singlenull = false;
                            escquote = false;
                            stringtext = new StringBuilder("");
			    return new Symbol(TokenConstants.ERROR, "String contains escaped null character."); 
                       }
                       if (singlenull == true) {
                            yybegin(YYINITIAL); 
                            escnull = false;
                            escquote = false;
                            singlenull = false;
                            stringtext = new StringBuilder("");
			    return new Symbol(TokenConstants.ERROR, "String contains null character."); 
                       }
                       if (escquote == false) {
                           yybegin(YYINITIAL); 
                            int stl = stringtext.length();
                            if (stl >= MAX_STR_CONST) {
                                 stringtext = new StringBuilder("");
			         return new Symbol(TokenConstants.ERROR, "String constant too long"); 
                            }
                            else {
		                String myyytext = stringtext.toString();
                                stringtext = new StringBuilder("");
			        return new Symbol(TokenConstants.STR_CONST, new StringSymbol(myyytext, myyytext.length(), 0)); 
                            }
                       }
                       else {
                            stringtext = stringtext.append("\"");
                            escquote = false;
                       }
                   }
					case -77:
						break;
					case 77:
						{  
                         if (escquote == true) {
                              escnull = true;
                              singlenull = false;
                              escquote = false;
                         }
                         else {
                              escnull = false;
                              singlenull = true;
                         }
                     }
					case -78:
						break;
					case 79:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -79:
						break;
					case 80:
						{return new Symbol(TokenConstants.TYPEID, new IdSymbol(yytext(), yytext().length(), 0)); }
					case -80:
						break;
					case 81:
						{return new Symbol(TokenConstants.FI); }
					case -81:
						break;
					case 82:
						{return new Symbol(TokenConstants.IF); }
					case -82:
						break;
					case 83:
						{return new Symbol(TokenConstants.IN); }
					case -83:
						break;
					case 84:
						{return new Symbol(TokenConstants.OF); }
					case -84:
						break;
					case 85:
						{return new Symbol(TokenConstants.LET); }
					case -85:
						break;
					case 86:
						{return new Symbol(TokenConstants.NEW); }
					case -86:
						break;
					case 87:
						{return new Symbol(TokenConstants.NOT); }
					case -87:
						break;
					case 88:
						{return new Symbol(TokenConstants.CASE); }
					case -88:
						break;
					case 89:
						{return new Symbol(TokenConstants.LOOP); }
					case -89:
						break;
					case 90:
						{return new Symbol(TokenConstants.ELSE); }
					case -90:
						break;
					case 91:
						{return new Symbol(TokenConstants.ESAC); }
					case -91:
						break;
					case 92:
						{return new Symbol(TokenConstants.THEN); }
					case -92:
						break;
					case 93:
						{return new Symbol(TokenConstants.POOL); }
					case -93:
						break;
					case 94:
						{return new Symbol(TokenConstants.CLASS); }
					case -94:
						break;
					case 95:
						{return new Symbol(TokenConstants.ISVOID); }
					case -95:
						break;
					case 96:
						{return new Symbol(TokenConstants.INHERITS); }
					case -96:
						break;
					case 98:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -97:
						break;
					case 99:
						{return new Symbol(TokenConstants.TYPEID, new IdSymbol(yytext(), yytext().length(), 0)); }
					case -98:
						break;
					case 100:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -99:
						break;
					case 101:
						{return new Symbol(TokenConstants.TYPEID, new IdSymbol(yytext(), yytext().length(), 0)); }
					case -100:
						break;
					case 102:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -101:
						break;
					case 103:
						{return new Symbol(TokenConstants.TYPEID, new IdSymbol(yytext(), yytext().length(), 0)); }
					case -102:
						break;
					case 104:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -103:
						break;
					case 105:
						{return new Symbol(TokenConstants.TYPEID, new IdSymbol(yytext(), yytext().length(), 0)); }
					case -104:
						break;
					case 106:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -105:
						break;
					case 107:
						{return new Symbol(TokenConstants.TYPEID, new IdSymbol(yytext(), yytext().length(), 0)); }
					case -106:
						break;
					case 108:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -107:
						break;
					case 109:
						{return new Symbol(TokenConstants.TYPEID, new IdSymbol(yytext(), yytext().length(), 0)); }
					case -108:
						break;
					case 110:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -109:
						break;
					case 111:
						{return new Symbol(TokenConstants.TYPEID, new IdSymbol(yytext(), yytext().length(), 0)); }
					case -110:
						break;
					case 112:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -111:
						break;
					case 113:
						{return new Symbol(TokenConstants.TYPEID, new IdSymbol(yytext(), yytext().length(), 0)); }
					case -112:
						break;
					case 114:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -113:
						break;
					case 115:
						{return new Symbol(TokenConstants.TYPEID, new IdSymbol(yytext(), yytext().length(), 0)); }
					case -114:
						break;
					case 116:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -115:
						break;
					case 117:
						{return new Symbol(TokenConstants.TYPEID, new IdSymbol(yytext(), yytext().length(), 0)); }
					case -116:
						break;
					case 118:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -117:
						break;
					case 119:
						{return new Symbol(TokenConstants.TYPEID, new IdSymbol(yytext(), yytext().length(), 0)); }
					case -118:
						break;
					case 120:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -119:
						break;
					case 121:
						{return new Symbol(TokenConstants.TYPEID, new IdSymbol(yytext(), yytext().length(), 0)); }
					case -120:
						break;
					case 122:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -121:
						break;
					case 123:
						{return new Symbol(TokenConstants.TYPEID, new IdSymbol(yytext(), yytext().length(), 0)); }
					case -122:
						break;
					case 124:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -123:
						break;
					case 125:
						{return new Symbol(TokenConstants.TYPEID, new IdSymbol(yytext(), yytext().length(), 0)); }
					case -124:
						break;
					case 126:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -125:
						break;
					case 127:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -126:
						break;
					case 128:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -127:
						break;
					case 129:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -128:
						break;
					case 130:
						{return new Symbol(TokenConstants.TYPEID, new IdSymbol(yytext(), yytext().length(), 0)); }
					case -129:
						break;
					case 131:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -130:
						break;
					case 132:
						{return new Symbol(TokenConstants.TYPEID, new IdSymbol(yytext(), yytext().length(), 0)); }
					case -131:
						break;
					case 133:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -132:
						break;
					case 134:
						{return new Symbol(TokenConstants.TYPEID, new IdSymbol(yytext(), yytext().length(), 0)); }
					case -133:
						break;
					case 135:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -134:
						break;
					case 136:
						{return new Symbol(TokenConstants.TYPEID, new IdSymbol(yytext(), yytext().length(), 0)); }
					case -135:
						break;
					case 137:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -136:
						break;
					case 138:
						{return new Symbol(TokenConstants.TYPEID, new IdSymbol(yytext(), yytext().length(), 0)); }
					case -137:
						break;
					case 139:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -138:
						break;
					case 140:
						{return new Symbol(TokenConstants.TYPEID, new IdSymbol(yytext(), yytext().length(), 0)); }
					case -139:
						break;
					case 141:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -140:
						break;
					case 142:
						{return new Symbol(TokenConstants.TYPEID, new IdSymbol(yytext(), yytext().length(), 0)); }
					case -141:
						break;
					case 143:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -142:
						break;
					case 144:
						{return new Symbol(TokenConstants.TYPEID, new IdSymbol(yytext(), yytext().length(), 0)); }
					case -143:
						break;
					case 145:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -144:
						break;
					case 146:
						{return new Symbol(TokenConstants.TYPEID, new IdSymbol(yytext(), yytext().length(), 0)); }
					case -145:
						break;
					case 147:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -146:
						break;
					case 148:
						{return new Symbol(TokenConstants.TYPEID, new IdSymbol(yytext(), yytext().length(), 0)); }
					case -147:
						break;
					case 149:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -148:
						break;
					case 150:
						{return new Symbol(TokenConstants.TYPEID, new IdSymbol(yytext(), yytext().length(), 0)); }
					case -149:
						break;
					case 151:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -150:
						break;
					case 152:
						{return new Symbol(TokenConstants.TYPEID, new IdSymbol(yytext(), yytext().length(), 0)); }
					case -151:
						break;
					case 153:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -152:
						break;
					case 154:
						{return new Symbol(TokenConstants.TYPEID, new IdSymbol(yytext(), yytext().length(), 0)); }
					case -153:
						break;
					case 155:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -154:
						break;
					case 156:
						{return new Symbol(TokenConstants.TYPEID, new IdSymbol(yytext(), yytext().length(), 0)); }
					case -155:
						break;
					case 157:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -156:
						break;
					case 158:
						{return new Symbol(TokenConstants.TYPEID, new IdSymbol(yytext(), yytext().length(), 0)); }
					case -157:
						break;
					case 159:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -158:
						break;
					case 160:
						{return new Symbol(TokenConstants.TYPEID, new IdSymbol(yytext(), yytext().length(), 0)); }
					case -159:
						break;
					case 161:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -160:
						break;
					case 162:
						{return new Symbol(TokenConstants.TYPEID, new IdSymbol(yytext(), yytext().length(), 0)); }
					case -161:
						break;
					case 163:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -162:
						break;
					case 164:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -163:
						break;
					case 165:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -164:
						break;
					case 166:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -165:
						break;
					case 167:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -166:
						break;
					case 168:
						{return new Symbol(TokenConstants.TYPEID, new IdSymbol(yytext(), yytext().length(), 0)); }
					case -167:
						break;
					case 169:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -168:
						break;
					case 170:
						{return new Symbol(TokenConstants.TYPEID, new IdSymbol(yytext(), yytext().length(), 0)); }
					case -169:
						break;
					case 171:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -170:
						break;
					case 172:
						{return new Symbol(TokenConstants.TYPEID, new IdSymbol(yytext(), yytext().length(), 0)); }
					case -171:
						break;
					case 173:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -172:
						break;
					case 174:
						{return new Symbol(TokenConstants.TYPEID, new IdSymbol(yytext(), yytext().length(), 0)); }
					case -173:
						break;
					case 175:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -174:
						break;
					case 176:
						{return new Symbol(TokenConstants.OBJECTID, new StringSymbol(yytext(), yytext().length(), 0)); }
					case -175:
						break;
					default:
						yy_error(YY_E_INTERNAL,false);
					case -1:
					}
					yy_initial = true;
					yy_state = yy_state_dtrans[yy_lexical_state];
					yy_next_state = YY_NO_STATE;
					yy_last_accept_state = YY_NO_STATE;
					yy_mark_start();
					yy_this_accept = yy_acpt[yy_state];
					if (YY_NOT_ACCEPT != yy_this_accept) {
						yy_last_accept_state = yy_state;
						yy_mark_end();
					}
				}
			}
		}
	}
}
