/*
 *  cool.cup
 *              Parser definition for the COOL language.
 *
 */
 
import java_cup.runtime.*;

/* Stuff enclosed in {: :} is copied verbatim to the class containing
all parser actions.  All the extra variables/functions you want to use
in the semantic actions should go here.  Don't remove or modify anything
that was there initially.  */

action code {: 

    int curr_lineno() {
	return ((CoolTokenLexer)parser.getScanner()).curr_lineno();
    }

    AbstractSymbol curr_filename() {
	return ((CoolTokenLexer)parser.getScanner()).curr_filename();
    }
:} 

/************************************************************************/
/*                DONT CHANGE ANYTHING IN THIS SECTION                  */

parser code {:
    int omerrs = 0;

    public void syntax_error(Symbol cur_token) {
        int lineno = action_obj.curr_lineno();
	String filename = action_obj.curr_filename().getString();
        System.err.print("\"" + filename + "\", line " + lineno + 
		         ": parse error at or near ");
        Utilities.printToken(cur_token);
	omerrs++;
	if (omerrs>50) {
	   System.err.println("More than 50 errors");
	   System.exit(1);
	}
    }

    public void unrecovered_syntax_error(Symbol cur_token) {
    }
:}

/* Declare the terminals; a few have types for associated lexemes.  The
token ERROR is never used in the parser; thus, it is a parse error when
the lexer returns it.  */

terminal CLASS, ELSE, FI, IF, IN, INHERITS, LET, LET_STMT, LOOP, POOL, THEN, WHILE;
terminal CASE, ESAC, OF, DARROW, NEW, ISVOID;
terminal ASSIGN, NOT, LE, ERROR;
terminal PLUS, DIV, MINUS, MULT, EQ, LT, DOT, NEG, COMMA, SEMI, COLON;
terminal LPAREN, RPAREN, AT, LBRACE, RBRACE;
terminal AbstractSymbol STR_CONST, INT_CONST;
terminal Boolean BOOL_CONST;
terminal AbstractSymbol TYPEID, OBJECTID;

/*  DON'T CHANGE ANYTHING ABOVE THIS LINE, OR YOUR PARSER WONT WORK       */
/**************************************************************************/

   /* Complete the nonterminal list below, giving a type for the semantic
      value of each non terminal. (See the CUP documentation for details. */


nonterminal programc program;
nonterminal Classes class_list;
nonterminal class_c class;
nonterminal Features feature_list;
nonterminal Feature feature;
nonterminal Feature feature_attr;
nonterminal Feature feature_method;
nonterminal formalc formal;
nonterminal Formals formal_list;
nonterminal Expression expression;
nonterminal Expressions expr_comma_list;
nonterminal Expressions expr_semi_list;
nonterminal branch branch_single;
nonterminal Cases case_list;
nonterminal Expression let_expr;

/* Precedence declarations go here. */

precedence left ERROR;
precedence nonassoc EQ, LE, LT;
precedence left PLUS, MINUS;
precedence left MULT, DIV;
precedence left NEG, ISVOID;
precedence left DOT;
precedence nonassoc NEG, ISVOID;

program	
	::= class_list:cl
	    {: RESULT = new programc(curr_lineno(), cl); :}
        ;

class_list
	/* single class */
	::= class:c SEMI
	    {: RESULT = (new Classes(curr_lineno())).appendElement(c); :}
	/* several classes */
	| class_list:cl class:c SEMI
	    {: RESULT = cl.appendElement(c); :}
        | error SEMI
	    {: RESULT = (new Classes(curr_lineno())); :}
	;

/* If no parent is specified, the class inherits from the Object class */
class
	::= CLASS TYPEID:t1 INHERITS TYPEID:t2 LBRACE feature_list:fl RBRACE
	    {: RESULT = new class_c(curr_lineno(), t1, t2, fl, curr_filename()); :}
	| CLASS TYPEID:t3 LBRACE feature_list:fl RBRACE
	    {: RESULT = new class_c(curr_lineno(), t3, 
		                   AbstractTable.idtable.addString("Object"), 
				   fl, curr_filename()); :}
	| CLASS TYPEID:t4 INHERITS TYPEID:t5 LBRACE RBRACE
	    {: RESULT = new class_c(curr_lineno(), t4, t5, 
                        new Features(curr_lineno()), curr_filename()); :}
	| CLASS TYPEID:t6 LBRACE RBRACE
	    {: RESULT = new class_c(curr_lineno(), t6, 
                        AbstractTable.idtable.addString("Object"),
                        new Features(curr_lineno()), curr_filename()); :}
	;

formal
        ::= OBJECTID:o COLON TYPEID:t
            {: RESULT = new formalc(curr_lineno(), o, t); :}
        ;

formal_list 
        ::= formal:f1
            {: RESULT = new Formals(curr_lineno()).appendElement(f1); :}
        | formal_list:fl COMMA formal:f2 
            {: RESULT = fl.appendElement(f2); :}
        ;

/* Feature list may be empty, but no empty features in list. */
feature_list
	::= feature:f1 SEMI
	    {: RESULT = new Features(curr_lineno()).appendElement(f1); :}
        | feature_list:fl feature:f2 SEMI 
	    {: RESULT = fl.appendElement(f2); :}
        | error SEMI
	    {: RESULT = new Features(curr_lineno()); :}
	;

feature_attr
        ::= OBJECTID:o COLON TYPEID:t
            {: RESULT = new attr(curr_lineno(), o, t, new no_expr(curr_lineno())); :}
        | OBJECTID:o COLON TYPEID:t ASSIGN expression:e
            {: RESULT = new attr(curr_lineno(), o, t, e); :}
        ;

feature_method
        ::= OBJECTID:o LPAREN RPAREN COLON TYPEID:t LBRACE expression:e RBRACE
            {: RESULT = new method(curr_lineno(), o, new Formals(curr_lineno()), t, e); :}
        | OBJECTID:o LPAREN formal_list:fl RPAREN COLON TYPEID:t LBRACE expression:e RBRACE
            {: RESULT = new method(curr_lineno(), o, fl, t, e); :}
        ;

feature
        ::= feature_attr:a
            {: RESULT = a; :}
        | feature_method:m
            {: RESULT = m; :}
        ;

branch_single
        ::= OBJECTID:o COLON TYPEID:t DARROW expression:e SEMI
            {: RESULT = new branch(curr_lineno(), o, t, e); :} 
        ;

case_list 
        ::= branch_single:b1
            {: RESULT = new Cases(curr_lineno()).appendElement(b1); :}
        | case_list:cl branch_single:b2
            {: RESULT = cl.appendElement(b2); :}
        ;

expr_semi_list
        ::= expression:e SEMI
            {: RESULT = new Expressions(curr_lineno()).appendElement(e); :}
        | expr_semi_list:ecl expression:e SEMI
            {: RESULT = ecl.appendElement(e); :}
        ;

expr_comma_list
        ::= expression:e1 
            {: RESULT = new Expressions(curr_lineno()).appendElement(e1); :}
        | expr_comma_list:ecl COMMA expression:e2
            {: RESULT = ecl.appendElement(e2); :}
        | error COMMA
            {: RESULT = new Expressions(curr_lineno()); :}
        ;

expression
        ::= expression:e1 PLUS expression:e2
            {: RESULT = new plus(curr_lineno(), e1, e2); :}
        | expression:e1 MINUS expression:e2
            {: RESULT = new sub(curr_lineno(), e1, e2); :}
        | expression:e1 MULT expression:e2
            {: RESULT = new mul(curr_lineno(), e1, e2); :}
        | expression:e1 DIV expression:e2
            {: RESULT = new divide(curr_lineno(), e1, e2); :}
        | BOOL_CONST:b
            {: RESULT = new bool_const(curr_lineno(), b); :}
        | STR_CONST:s
            {: RESULT = new string_const(curr_lineno(), s); :}
        | INT_CONST:i
            {: RESULT = new int_const(curr_lineno(), i); :}
        | OBJECTID:o
            {: RESULT = new object(curr_lineno(), o); :}
        | LPAREN expression:e RPAREN
            {: RESULT = e; :}
        | NEG expression:e
            {: RESULT = new neg(curr_lineno(), e); :}
        | NOT expression:e
            {: RESULT = new comp(curr_lineno(), e); :}
        | expression:e1 EQ expression:e2
            {: RESULT = new eq(curr_lineno(), e1, e2); :}
        | expression:e1 LT expression:e2
            {: RESULT = new lt(curr_lineno(), e1, e2); :}
        | expression:e1 LE expression:e2
            {: RESULT = new leq(curr_lineno(), e1, e2); :}
        | ISVOID expression:e
            {: RESULT = new isvoid(curr_lineno(), e); :}
        | NEW TYPEID:t
            {: RESULT = new new_(curr_lineno(), t); :}
        | IF expression:e1 THEN expression:e2 ELSE expression:e3 FI
            {: RESULT = new cond(curr_lineno(), e1, e2, e3); :}
        | WHILE expression:e1 LOOP expression:e2 POOL
            {: RESULT = new loop(curr_lineno(), e1, e2); :}
        | WHILE expression:e1 LOOP error 
            {: RESULT = new no_expr(curr_lineno()); :}
        | OBJECTID:o ASSIGN expression:e
            {: RESULT = new assign(curr_lineno(), o, e); :}  
        | LBRACE expr_semi_list:ecl RBRACE
            {: RESULT = new block(curr_lineno(), ecl); :}
        | OBJECTID:o LPAREN expr_comma_list:ecl RPAREN
            {: RESULT = new dispatch(curr_lineno(), 
               new object(curr_lineno(),AbstractTable.idtable.addString("self")), 
               o, ecl); :}
        | OBJECTID:o LPAREN  RPAREN
            {: RESULT = new dispatch(curr_lineno(), 
               new object(curr_lineno(),AbstractTable.idtable.addString("self")), 
               o, new Expressions(curr_lineno())); :}
        | expression:e AT TYPEID:t DOT OBJECTID:o LPAREN expr_comma_list:ecl RPAREN
            {: RESULT = new static_dispatch(curr_lineno(), e, t, o, ecl); :}
        | expression:e AT TYPEID:t DOT OBJECTID:o LPAREN RPAREN
            {: RESULT = new static_dispatch(curr_lineno(), e, t, o, 
               new Expressions(curr_lineno())); :}
        | expression:e DOT OBJECTID:o LPAREN RPAREN
            {: RESULT = new dispatch(curr_lineno(), e, o, 
               new Expressions(curr_lineno()) ); :}
        | expression:e DOT OBJECTID:o LPAREN expr_comma_list:ecl RPAREN
            {: RESULT = new dispatch(curr_lineno(), e, o, ecl); :}
        | CASE expression:e OF case_list:cl ESAC
            {: RESULT = new typcase(curr_lineno(), e, cl); :}
        | LET OBJECTID:o COLON TYPEID:t IN expression:e1
            {: RESULT = new let(curr_lineno(), o, t, new no_expr(curr_lineno()), e1); :}
        | LET OBJECTID:o COLON TYPEID:t ASSIGN expression:e2 IN expression:e3
            {: RESULT = new let(curr_lineno(), o, t, e2, e3); :}
        | LET OBJECTID:o COLON TYPEID:t COMMA expression:e1
            {: RESULT = new let(curr_lineno(), o, t, new no_expr(curr_lineno()), e1); :}
        | LET OBJECTID:o COLON TYPEID:t ASSIGN expression:e2 COMMA expression:e3
            {: RESULT = new let(curr_lineno(), o, t, e2, e3); :}

        | OBJECTID:o COLON TYPEID:t COMMA expression:e1
            {: RESULT = new let(curr_lineno(), o, t, new no_expr(curr_lineno()), e1); :}
        | OBJECTID:o COLON TYPEID:t ASSIGN expression:e2 COMMA expression:e3
            {: RESULT = new let(curr_lineno(), o, t, e2, e3); :}
 
        | OBJECTID:o COLON TYPEID:t IN expression:e1
            {: RESULT = new let(curr_lineno(), o, t, new no_expr(curr_lineno()), e1); :}
        | OBJECTID:o COLON TYPEID:t ASSIGN expression:e2 IN expression:e3
            {: RESULT = new let(curr_lineno(), o, t, e2, e3); :}

        | error
            {: RESULT = new no_expr(curr_lineno()); :}
        ;
